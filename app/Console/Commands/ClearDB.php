<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

use DB;

class ClearDB extends Command
{
    protected $signature = 'cleardb';

    protected $description = 'Import de archivos excel';

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        if (!$this->confirm('Está seguro que desea eliminar todas las tablas de la base de datos?')) {
            $this->line('El comando de borrado de tablas fue suspendido');
            exit();
        }
        
        $tables = DB::connection()->getDoctrineSchemaManager()->listTableNames();
        
        DB::beginTransaction();
        $mysql = env('DB_CONNECTION') == 'mysql';
        if ($mysql) {
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');
            foreach ($tables as $table) {
                DB::statement("DROP TABLE $table");
            }
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        } else {
            foreach ($tables as $table) {
                DB::statement("DROP TABLE $table CASCADE");
            }
        }
        DB::commit();

        $this->info('Culminacion del borrado');
    }

    public function getOutput()
    {
        return $this->output;
    }
}