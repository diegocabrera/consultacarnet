<?php

namespace App\Console\Commands;

use DB;
use Excel;
use Carbon\Carbon;
use Storage;

use App\Modules\Consultapsuv\Models\Centros;
use App\Modules\Consultapsuv\Models\Rep;

class importClass
{
    protected $errores = [];

    protected $i = 0;
    public $console = false;
    public $dev = false;
    public $bar = false;


    public function progressBar($count) 
    {
        if ($this->console === false) {
            return false;
        }
        $this->bar = $this->console->getOutput()->createProgressBar($count);
        return $this->bar;
    }
    
    public function advance() 
    {
        if ($this->console === false) {
            return false;
        }
       
        return $this->bar->advance();
    }

    public function info($msj) 
    {
        if ($this->console === false) {
            echo $msj . "<br /> \n";
            return false;
        }
        return $this->console->info($msj);
    }

    public function warn($msj) 
    {
        if ($this->console === false) {
            echo $msj . "<br /> \n";
            return false;
        }
        return $this->console->warn($msj);
    }

    public function error($msj) 
    {
        if ($this->console === false) {
            echo $msj . "<br /> \n";
            return false;
        }
        return $this->console->error($msj);
    }

    public function init($paso)
    {
        $inicio = Carbon::now();
        $this->info("Inicio: " . $inicio->format('d/m/Y H:i:s'));

        $this->_init();
        
        switch ($paso) {
            case '1':
                $this->procesar_rep();
                break;
            
            case '2':
                $this->procesar_rep_csv();
                break;
            
            case '3':
                $this->procesar_datos_centros_electorales();
                break;
            
            case '4':
                $this->procesar_circulos_lucha();
                break;

            case '5':
                $this->procesar_circulos_lucha();
                break;

            case '6':
                $this->procesar_lideres_calle();
                break;
            
            case '7':
                $this->procesar_ubch();
                break;
            
            case '8':
                $this->procesar_unopordiez();
                break;
            
            case '9':
                $this->procesar_alcaldia();
                break;
            
            case '10':
                $this->procesar_fondo_bolivar();
                break;
            
            case '12':
                $this->procesar_gobernacion();
                break;

            case 'duplicados':
                $this->eliminar_duplicados_rep();
                break;
            
            case 'todo':
                $this->procesar_datos_centros_electorales();
                $this->procesar_circulos_lucha();
                $this->procesar_circulos_lucha();
                $this->procesar_lideres_calle();
                $this->procesar_ubch();
                $this->procesar_unopordiez();
                $this->procesar_alcaldia();
                $this->procesar_fondo_bolivar();
                $this->procesar_gobernacion();
                break;

            case 'excel':
                $this->generar_excel();
                break;
            case 'excel_parroquia':
                $this->generar_excel_parroquia();
                break;

            default:
                $this->info("Debe escoger una opcion del 1 al 3");
                break;
        }


        $fin = Carbon::now();
        $tiempo = $fin->diffForHumans($inicio);

        $this->info("");
        $this->info("Final: " . $fin->format('d/m/Y H:i:s'));
        $this->info("Tiempo del proceso " . $tiempo);
    }

    public function _init()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '-1');
        
        //$start_row = $this->argument('start_row');
        //$start_row = 11;

        //config(['excel.import.startRow' => $start_row ]);

        $this->initVars();
    }

    public function initVars() 
    {
        return '';
    }

    protected function truncar($tablas) 
    {
        //$this->error('Truncate Desabilitado');
        //exit();
        DB::statement('SET foreign_key_checks = 0;');
        foreach ($tablas as $tabla) {
            DB::statement("TRUNCATE $tabla;");
        }
        DB::statement('SET foreign_key_checks = 1;');
    }

    protected function definicion($lista)
    {
        $res = [];
        foreach ($lista as $ele) {
            $res[str_slug($ele->nombre)] = $ele->id;
        }

        return $res;
    }

    protected function procesar_string($s)
    {
        $s = preg_replace('/\s+/', ' ',$s);
        $s = strtolower($s);
        $s = trim($s);
        $s = utf8_encode($s);
        $s = strtr($s, 'ÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ', 'àáâãäçèéêëìíîïñòóôõöùúûüý');
        return $s;
    }

    protected function procesar_telefono($s)
    {
        $s = preg_replace('/[^0-9]/', '', $s);;
        $s = ltrim($s, '0');
        if (strlen($s) < 10) {
            return '';
        }

        if (strlen($s) > 14) {
            $this->warn($s . ' no posee el formato.');
            return '';
        }

        if (preg_match('/^(\d{3})\-?(\d{7})$/', $s, $match)) {
            if ($match[1][0] != '0') {
                $match[1] = '0' . $match[1];
            }
            $s = $match[1] . '-' . $match[2];
        } elseif ($s != '') {
            return '';
        }
        
        return $s;
    }

    protected function procesar_cedula($s)
    {
        $s = preg_replace('/\s+/', ' ',$s);
        $s = strtolower($s);
        $s = trim($s);
        $s = str_replace(['v', '.', ',', '-', 'e'], '', $s);
        $s = intval($s);
        return $s;
    }

    public function procesar_rep()
    {
        $this->i = 0;

        $this->info("");
        $this->info("Importando REP");

        if ($this->dev) {
            $this->info("Modo DEV");
            $this->progressBar(8);
        } else {
            $this->progressBar(991668);
        }
        
        $this->truncar([
            'rep'
        ]);

        //dd($ClientesCategorias, $ClientesCondicionContribuyente, $ClientesRazonSocial, $ClientesRutas);
        $archivo = 'storage/app/rep' . ($this->dev ? '_dev' : '') . '.xlsx';
        Excel::load($archivo, function($reader) {
            //$reader->setDateFormat('Y-m-d');
            //$this->info("");

            // DB::beginTransaction();
            
            foreach ($reader->get() as $linea) {
                $this->guardar_rep($linea);
            }

            //DB::commit();
        });
    }

    public function procesar_rep_csv()
    {
        $this->i = 0;

        $this->info("");
        $this->info("Importando REP desde csv");
        if ($this->dev) {
            $this->info("Modo DEV");
            $this->progressBar(8);
        } else {
            $this->progressBar(991668);
        }
        
        $this->truncar([
            'rep'
        ]);

        $time = [
            time(),
            time()
        ];
        $vueltas = [0];
        $j = 0;

        $archivo = 'storage/app/rep' . ($this->dev ? '_dev' : '') . '.csv';

        DB::beginTransaction();
        if (($fichero = fopen($archivo, "r")) !== FALSE) {
            while (($datos = fgetcsv($fichero, 1000, ';')) !== FALSE) {
                if ($this->i === 0) {
                    $this->i++;
                    continue;
                }

                $linea = (Object) [];
                
                $linea->nacionalidad  = $datos[0];
                $linea->dni           = $datos[1];
                $linea->nombre        = $datos[2];
                $linea->genero        = $datos[3];
                $linea->nacimiento    = $datos[4];
                $linea->centro_id     = $datos[5];
                $linea->municipio     = $datos[6];
                $linea->parroquia     = $datos[7];
                $linea->nombre_centro = $datos[8];
                $linea->direccion     = $datos[9];
                
                $linea->fch           = $datos[10];
                $linea->fcm           = $datos[11];
                $linea->cp            = $datos[12];
                $linea->psuv          = $datos[13];
                
                $linea->movil         = $datos[14];
                $linea->telefono      = $datos[15];
                $linea->telf          = $datos[16];

                $this->guardar_rep($linea);

                $time[1] = time();
                $vueltas[$j]++;
                
                if ($time[1] - $time[0] > 60) {
                    $time[0] = $time[1];
                    
                    $this->info("");
                    $this->info("Promedio de registros por minuto: " . (array_sum($vueltas) / count($vueltas)));

                    $j++;
                    $vueltas[$j] = 0;
                }
            }
        }
        DB::commit();
    }

    protected function guardar_rep($linea)
    {
        $linea->nacionalidad  = $this->procesar_string($linea->nacionalidad);
        $linea->dni           = intval($linea->dni);
        $linea->nombre        = ucwords($this->procesar_string($linea->nombre));
        $linea->genero        = $this->procesar_string($linea->genero);
        //$linea->nacimiento    = $linea->nacimiento;
        $linea->centro_id     = intval($linea->centro_id);
        $linea->municipio     = ucwords($this->procesar_string($linea->municipio));
        $linea->parroquia     = ucwords($this->procesar_string($linea->parroquia));
        $linea->nombre_centro = ucwords($this->procesar_string($linea->nombre_centro));
        $linea->direccion     = ucfirst($this->procesar_string($linea->direccion));
        
        $linea->fch           = $this->procesar_string($linea->fch)  == 's' ? 1 : 0;
        $linea->fcm           = $this->procesar_string($linea->fcm)  == 's' ? 1 : 0;
        $linea->cp            = $this->procesar_string($linea->cp)   == 's' ? 1 : 0;
        $linea->psuv          = $this->procesar_string($linea->psuv) == 's' ? 1 : 0;
        
        $linea->movil         = $this->procesar_telefono($linea->movil);
        $linea->telefono      = $this->procesar_telefono($linea->telefono);
        $linea->telf          = $this->procesar_telefono($linea->telf);
        
        $telefono = [$linea->movil, $linea->telefono, $linea->telf];
        $telefono = array_filter($telefono, 'strlen');

        $centro = Centros::find($linea->centro_id);
        
        if ($centro) {
            $centro->nombre = $linea->nombre_centro;
            $centro->municipio = $linea->municipio;
            $centro->parroquia = $linea->parroquia;
            $centro->direccion = $linea->direccion;
            $centro->save();
        }
        
        try {
            $persona = Rep::create([
                'nacionalidad' => $linea->nacionalidad,
                'dni'          => $linea->dni,
                'nombre'       => $linea->nombre,
                'genero'       => $linea->genero,
                'nacimiento'   => $linea->nacimiento,
                'centro_id'    => $linea->centro_id,
                'telefono'     => implode(',', $telefono),
                'fch'          => $linea->fch,
                'fcm'          => $linea->fcm,
                'cp'           => $linea->cp,
                'psuv'         => $linea->psuv
            ]);
        } catch (\Illuminate\Database\QueryException $e){
            $errorCode = $e->errorInfo[1];
            if($errorCode == 1062){
                //$this->warn('Registro duplicado: ' . $linea->dni);
            }
        }

        $this->i++;
        $this->advance();
    }

    public function procesar_datos_centros_electorales()
    {
        $this->i = 0;

        $this->info("");
        $this->info("Importando centros electorales");

        $archivo = 'storage/app/datos_centros_electorales.csv';

        DB::beginTransaction();
        $this->csv($archivo, function($linea) {
            $centro = Centros::find($linea['codigo']);
            
            if ($centro) {
                $centro->nombre     = $this->procesar_string($linea['nombre']);
                $centro->direccion  = $this->procesar_string($linea['direccion']);
                $centro->mesas      = $linea['mesa'];
                $centro->tomo       = $linea['tomo'];
                $centro->votantes   = $linea['electores'];
                $centro->tecnologia = $this->procesar_string($linea['tecnologia']);
                $centro->cac        = $linea['cac'];
                $centro->ccv        = $linea['ccv'];
                $centro->osi        = $linea['osi'];
                $centro->oi         = $linea['oi'];
                $centro->tssi       = $linea['ts_si'];
                $centro->save();
            }
        });
        DB::commit();
    }

    public function procesar_circulos_lucha()
    {
        $this->info("");
        $this->info("Importando circulos de lucha");

        $archivo = 'storage/app/jefes_ubch-jclp.csv';

        DB::beginTransaction();
        $this->csv($archivo, function($linea) {
            $linea['cedula'] = $this->procesar_cedula($linea['cedula']);
            $persona = Rep::where('dni', $linea['cedula'])->first();
            
            if ($persona) {
                $persona->clp = 1;
                $persona->save();
            } else {
                $this->warn($this->i . ': La cedula ' . $linea['cedula'] . ' no se encuentra');
            }
        });
        DB::commit();

        \DB::table('rep')->update(['deleted_at' => null]);
    }

    public function procesar_lideres_calle()
    {
        $this->info("");
        $this->info("Importando lideres de calle");

        $archivo = 'storage/app/lideres_calle.csv';

        DB::beginTransaction();
        $this->csv($archivo, function($linea) {
            $linea['cedula'] = $this->procesar_cedula($linea['cedula']);
            $persona = Rep::where('dni', $linea['cedula'])->first();

            if ($persona) {
                $persona->lider_calle = 1;
                $persona->save();
            } else {
                $this->warn($this->i . ': La cedula ' . $linea['cedula'] . ' no se encuentra');
            }
        });
        DB::commit();
        \DB::table('rep')->update(['deleted_at' => null]);
    }
    
    public function procesar_ubch()
    {
        $this->info("");
        $this->info("Importando ubch");

        $archivo = 'storage/app/ubc_bolivar.csv';

        DB::beginTransaction();
        $this->csv($archivo, function($linea) {
            $linea['cedula'] = $this->procesar_cedula($linea['cedula']);
            $persona = Rep::where('dni', $linea['cedula'])->first();
            
            if ($persona) {
                $persona->ubch = 1;
                $persona->save();
            } else {
                $this->warn($this->i . ': La cedula ' . $linea['cedula'] . ' no se encuentra');
            }
        });
        DB::commit();
        \DB::table('rep')->update(['deleted_at' => null]);
    }

    public function procesar_unopordiez()
    {
        $this->info("");
        $this->info("Importando uno por diez (1 x 10)");

        $archivo = 'storage/app/ubc_resp1x10.csv';

        DB::beginTransaction();
        $this->csv($archivo, function($linea) {
            $linea['cedula'] = $this->procesar_cedula($linea['cedula']);
            $persona = Rep::where('dni', $linea['cedula'])->first();
            
            if ($persona) {
                $persona->unopordiez = 1;
                $persona->save();
            } else {
                $this->warn($this->i . ': La cedula ' . $linea['cedula'] . ' no se encuentra');
            }
        });
        DB::commit();
        \DB::table('rep')->update(['deleted_at' => null]);
    }

    public function procesar_alcaldia()
    {
        $this->info("");
        $this->info("Importando alcaldia");

        $archivo = 'storage/app/alcaldia.csv';

        DB::beginTransaction();
        $this->csv($archivo, function($linea) {
            $linea['cedula'] = $this->procesar_cedula($linea['cedula']);
            $persona = Rep::where('dni', $linea['cedula'])->first();
            
            if ($persona) {
                $persona->alcaldia = 1;
                $persona->save();
            } else {
                $this->warn($this->i . ': La cedula ' . $linea['cedula'] . ' no se encuentra');
            }
        });
        DB::commit();
        \DB::table('rep')->update(['deleted_at' => null]);
    }

    public function procesar_fondo_bolivar()
    {
        $this->info("");
        $this->info("Importando fondo bolivar");

        $archivo = 'storage/app/fondo_bolivar.csv';

        DB::beginTransaction();
        $this->csv($archivo, function($linea) {
            $linea['cedula'] = $this->procesar_cedula($linea['cedula']);
            $persona = Rep::where('dni', $linea['cedula'])->first();
            
            if ($persona) {
                $persona->fondobolivar = 1;
                $persona->save();
            } else {
                $this->warn($this->i . ': La cedula ' . $linea['cedula'] . ' no se encuentra');
            }
        });
        DB::commit();
        \DB::table('rep')->update(['deleted_at' => null]);
    }

    public function procesar_gobernacion()
    {
        $this->info("");
        $this->info("Importando fondo bolivar");

        $archivo = 'storage/app/gobernacion.csv';

        DB::beginTransaction();
        $this->csv($archivo, function($linea) {
            $linea['cedula'] = $this->procesar_cedula($linea['cedula']);
            $persona = Rep::where('dni', $linea['cedula'])->first();
            
            if ($persona) {
                $persona->gob = 1;
                $persona->save();
            } else {
                $this->warn($this->i . ': La cedula ' . $linea['cedula'] . ' no se encuentra');
            }
        });
        DB::commit();
        \DB::table('rep')->update(['deleted_at' => null]);
    }

    public function generar_excel()
    {
        //$data = Rep::select();
        //$centros = Centros::where('parroquia', 'like', '%hermosa%')->get();
        $centros = Centros::all();
        $this->progressBar(count($centros));
        foreach ($centros as $centro) {
            $data = \DB::select("SELECT
                CONCAT(UPPER(nacionalidad), dni) AS cedula,
                rep.nombre AS nombre,
                telefono AS telefono,
                IF (fch = 1, 'Si', '') AS fch,
                IF (fcm = 1, 'Si', '') AS fcm,
                IF (cp = 1, 'Si', '') AS cp,
                IF (psuv = 1, 'Si', '') AS psuv,
                IF (unopordiez = 1, 'Si', '') AS unopordiez,
                -- IF (ubch = 1, 'Si', '') AS ubch,
                -- IF (clp = 1, 'Si', '') AS clp,
                IF (lider_comunidad = 1, 'Si', '') AS lider_comunidad,
                IF (testigo_mesa = 1, 'Si', '') AS testigo_mesa,
                IF (clap = 1, 'Si', '') AS clap,
                IF (lider_calle = 1, 'Si', '') AS lider_calle,
                gob,
                fd,
                alcaldia,
                fondobolivar
                -- IF (gob = 1, 'Si', '') AS \"Trabaja en Gobernación\",
                -- IF (fd = 1, 'Si', '') AS \"Trabaja en Fundación del Niño\",
                -- IF (alcaldia = 1, 'Si', '') AS \"Trabaja en Alcaldia de Heres\",
                -- IF (fondobolivar = 1, 'Si', '') AS \"Trabaja en Fondo Bolívar\"
            FROM
                rep
            LEFT JOIN centros ON centros.id = centro_id
            where
                -- centros.parroquia like '%sabanita%'
                centro_id = " . $centro->id . "
            ORDER BY dni");
            
            
            Excel::create($centro->id, function($excel) use ($centro, $data) {
                $excel->setCreator('Informatica')->setCompany('GEB');
                
                $excel->setTitle($centro->nombre);
                $excel->setDescription($centro->municipio . ', ' . $centro->paroquia . ', ' . $centro->nombre);
                
                $excel->sheet('data', function($sheet) use ($centro, $data) {
                    $_data = [];
                    $cantidades = [
                        'lider_calle'     => 0,
                        'lider_comunidad' => 0,
                        'clap'            => 0,
                        'unopordiez'      => 0,
                        'gob'             => 0,
                        'fd'              => 0,
                        'alcaldia'        => 0,
                        'fondobolivar'    => 0,
                    ];

                    foreach ($data as $d) {
                        $_data[] = [
                            $d->cedula,
                            str_replace('Ð', 'ñ', $d->nombre) . 
                                ($d->gob ? ' (G)' : '') . 
                                ($d->fd ? ' (F)' : '') . 
                                ($d->alcaldia ? ' (A)' : '') . 
                                ($d->fondobolivar ? ' (FB)' : ''),
                            $d->telefono,
                            $d->lider_calle,
                            $d->lider_comunidad,
                            $d->clap,
                            $d->unopordiez,
                            // $d->fch,
                            // $d->fcm,
                            // $d->cp,
                            // $d->psuv,
                            // $d->testigo_mesa,
                        ];

                        if ($d->lider_calle){
                            $cantidades['lider_calle']++;
                        }

                        if ($d->lider_comunidad){
                            $cantidades['lider_comunidad']++;
                        }

                        if ($d->clap){
                            $cantidades['clap']++;
                        }

                        if ($d->unopordiez){
                            $cantidades['unopordiez']++;
                        }

                        if ($d->gob){
                            $cantidades['gob']++;
                        }
                        if ($d->fd){
                            $cantidades['fd']++;
                        }
                        if ($d->alcaldia){
                            $cantidades['alcaldia']++;
                        }
                        if ($d->fondobolivar){
                            $cantidades['fondobolivar']++;
                        }
                    }
                    
                    $campos = [
                        'Cedula',
                        'Nombre',
                        'Telefono',
                        "Lider de\rCalle",
                        "Lider de\rComunidad",
                        'Clap',
                        'Responsable 1x10',
                        // 'Firmó contra Chavez',
                        // 'Firmó contra Maduro',
                        // 'Carnet de la Patria',
                        // 'PSUV',
                        // 'Testigo de Mesa',
                    ];

                    
                    $sheet->cell('A2', function($cell) use ($centro) {
                        $cell->setValue($centro->municipio . ' - ' . $centro->parroquia . ' - ' . ucwords($centro->nombre));
                    });

                    $sheet->cell('A3', function($cell) use ($centro, $data) {
                        $cell->setValue('Cantidad de electores: ' . count($data));
                    });

                    $sheet->cell('D2', function($cell) use ($centro, $cantidades) {
                        $cell->setValue('Lideres de Calle: ' . $cantidades['lider_calle']);
                    });
                    $sheet->cell('D3', function($cell) use ($centro, $cantidades) {
                        $cell->setValue('Lideres de Comunidad: ' . $cantidades['lider_comunidad']);
                    });

                    $sheet->cell('F2', function($cell) use ($centro, $cantidades) {
                        $cell->setValue('Claps: ' . $cantidades['clap']);
                    });
                    $sheet->cell('F3', function($cell) use ($centro, $cantidades) {
                        $cell->setValue('Responsables 1x10: ' . $cantidades['unopordiez']);
                    });

                    $sheet->cell('D4', function($cell) use ($centro, $cantidades) {
                        $cell->setValue('Gobernación: ' . $cantidades['gob']);
                    });
                    $sheet->cell('D5', function($cell) use ($centro, $cantidades) {
                        $cell->setValue('Fundación del Niño: ' . $cantidades['fd']);
                    });
                    $sheet->cell('F4', function($cell) use ($centro, $cantidades) {
                        $cell->setValue('Alcaldia: ' . $cantidades['alcaldia']);
                    });
                    $sheet->cell('F5', function($cell) use ($centro, $cantidades) {
                        $cell->setValue('Fondo Bolívar: ' . $cantidades['fondobolivar']);
                    });

                    $i = 7;
                    $sheet->row($i, $campos);
                    $sheet->row($i++, function($row) {
                        $row->setBackground('#989898');
                        $row->setAlignment('center');
                        $row->setValignment('center');
                        
                        $row->setFont(array(
                            //'family'     => 'Calibri',
                            //'size'       => '16',
                            'bold'       => true,
                            'italic'     => true,
                        ));
                        
                        // const BORDER_NONE				= 'none';
                        // const BORDER_DASHDOT			    = 'dashDot';
                        // const BORDER_DASHDOTDOT			= 'dashDotDot';
                        // const BORDER_DASHED				= 'dashed';
                        // const BORDER_DOTTED				= 'dotted';
                        // const BORDER_DOUBLE				= 'double';
                        // const BORDER_HAIR				= 'hair';
                        // const BORDER_MEDIUM				= 'medium';
                        // const BORDER_MEDIUMDASHDOT		= 'mediumDashDot';
                        // const BORDER_MEDIUMDASHDOTDOT	= 'mediumDashDotDot';
                        // const BORDER_MEDIUMDASHED		= 'mediumDashed';
                        // const BORDER_SLANTDASHDOT		= 'slantDashDot';
                        // const BORDER_THICK				= 'thick';
                        // const BORDER_THIN				= 'thin';
                    });
                    
                    foreach ($_data as $d) {
                        $sheet->row($i, $d);
                        $sheet->row($i, function($row) use ($i) {
                            $row->setBackground($i % 2 == 0 ? '#FFFFFF' : '#e8e8e8');
                        });
                        
                        $i++;
                    }
                    
                    $sheet->setAutoFilter('A7:G7');
                    $sheet->setBorder('A7:G' . ($i - 1), 'thin');
                    
                    $i = 7;
                    foreach ($_data as $d) {
                        for ($j = 0, $c = count($d); $j < count($d); $j++) { 
                            $sheet->cell(chr(65 + $j) . ($i + 1), function($cell) {
                                $cell->setBorder('thin', 'thick', 'thin', 'thick');
                            }); 
                        }
                        
                        $i++;
                    }

                    $sheet->setBorder('A7:G7', 'thick');

                    $sheet->setPageMargin([
                        0.15, 0.15, 0.15, 0.15
                    ]);

                    $sheet->setWidth([
                        'A' => 11,
                        'B' => 45.5,
                        'C' => 26.5,
                        'D' => 10.5,
                        'E' => 14,
                        'F' => 9,
                        'G' => 17,
                    ]);

                    $sheet->setHeight([
                        7 => 30
                    ]);
                });
            })->store('xlsx', storage_path('app/xlsx/' . $centro->municipio . '/' . $centro->parroquia . '/'));
            $this->advance();
        }
    }

    public function generar_excel_parroquia()
    {
        $data = \DB::select("SELECT
            CONCAT(UPPER(nacionalidad), dni) AS cedula,
            rep.nombre AS nombre,
            telefono AS telefono,
            centros.nombre as centro,
            IF (fch = 1, 'Si', '') AS fch,
            IF (fcm = 1, 'Si', '') AS fcm,
            IF (cp = 1, 'Si', '') AS cp,
            IF (psuv = 1, 'Si', '') AS psuv,
            IF (unopordiez = 1, 'Si', '') AS unopordiez,
            IF (lider_comunidad = 1, 'Si', '') AS lider_comunidad,
            IF (testigo_mesa = 1, 'Si', '') AS testigo_mesa,
            IF (clap = 1, 'Si', '') AS clap,
            IF (lider_calle = 1, 'Si', '') AS lider_calle,
            gob,
            fd,
            alcaldia,
            fondobolivar
        FROM
            rep
        LEFT JOIN centros ON centros.id = centro_id
        where
            centros.parroquia like '%sabanita%'
        ORDER BY dni");
        
        
        Excel::create('Pq Sabanita', function($excel) use ($data) {
            $excel->setCreator('Informatica')->setCompany('GEB');
            
            $excel->setTitle('Mp. Heres - Pq. Vista Hermosa');
            $excel->setDescription('Mp. Heres, Pq. Vista Hermosa');
            
            $excel->sheet('data', function($sheet) use ($data) {
                $_data = [];
                $cantidades = [
                    'lider_calle'     => 0,
                    'lider_comunidad' => 0,
                    'clap'            => 0,
                    'unopordiez'      => 0,
                    'gob'             => 0,
                    'fd'              => 0,
                    'alcaldia'        => 0,
                    'fondobolivar'    => 0,
                ];

                foreach ($data as $d) {
                    $_data[] = [
                        $d->cedula,
                        str_replace('Ð', 'ñ', $d->nombre) . 
                            ($d->gob ? ' (G)' : '') . 
                            ($d->fd ? ' (F)' : '') . 
                            ($d->alcaldia ? ' (A)' : '') . 
                            ($d->fondobolivar ? ' (FB)' : ''),
                        $d->telefono,
                        $d->centro,
                        $d->lider_calle,
                        $d->lider_comunidad,
                        $d->clap,
                        $d->unopordiez,
                        // $d->fch,
                        // $d->fcm,
                        // $d->cp,
                        // $d->psuv,
                        // $d->testigo_mesa,
                    ];

                    if ($d->lider_calle){
                        $cantidades['lider_calle']++;
                    }

                    if ($d->lider_comunidad){
                        $cantidades['lider_comunidad']++;
                    }

                    if ($d->clap){
                        $cantidades['clap']++;
                    }

                    if ($d->unopordiez){
                        $cantidades['unopordiez']++;
                    }

                    if ($d->gob){
                        $cantidades['gob']++;
                    }
                    if ($d->fd){
                        $cantidades['fd']++;
                    }
                    if ($d->alcaldia){
                        $cantidades['alcaldia']++;
                    }
                    if ($d->fondobolivar){
                        $cantidades['fondobolivar']++;
                    }
                }
                
                $campos = [
                    'Cedula',
                    'Nombre',
                    'Telefono',
                    'Centro',
                    "Lider de\rCalle",
                    "Lider de\rComunidad",
                    'Clap',
                    'Responsable 1x10',
                    // 'Firmó contra Chavez',
                    // 'Firmó contra Maduro',
                    // 'Carnet de la Patria',
                    // 'PSUV',
                    // 'Testigo de Mesa',
                ];

                
                $sheet->cell('A2', function($cell) {
                    $cell->setValue('Mp. Heres - Pq. Vista Hermosa');
                });

                $sheet->cell('A3', function($cell) use ($data) {
                    $cell->setValue('Cantidad de electores: ' . count($data));
                });

                $sheet->cell('C2', function($cell) use ($cantidades) {
                    $cell->setValue('Lideres de Calle: ' . $cantidades['lider_calle']);
                });
                $sheet->cell('C3', function($cell) use ($cantidades) {
                    $cell->setValue('Lideres de Comunidad: ' . $cantidades['lider_comunidad']);
                });

                $sheet->cell('D2', function($cell) use ($cantidades) {
                    $cell->setValue('Claps: ' . $cantidades['clap']);
                });
                $sheet->cell('D3', function($cell) use ($cantidades) {
                    $cell->setValue('Responsables 1x10: ' . $cantidades['unopordiez']);
                });

                $sheet->cell('C4', function($cell) use ($cantidades) {
                    $cell->setValue('Gobernación: ' . $cantidades['gob']);
                });
                $sheet->cell('C5', function($cell) use ($cantidades) {
                    $cell->setValue('Fundación del Niño: ' . $cantidades['fd']);
                });
                $sheet->cell('D4', function($cell) use ($cantidades) {
                    $cell->setValue('Alcaldia: ' . $cantidades['alcaldia']);
                });
                $sheet->cell('D5', function($cell) use ($cantidades) {
                    $cell->setValue('Fondo Bolívar: ' . $cantidades['fondobolivar']);
                });

                $i = 7;
                $sheet->row($i, $campos);
                $sheet->row($i++, function($row) {
                    $row->setBackground('#989898');
                    $row->setAlignment('center');
                    $row->setValignment('center');
                    
                    $row->setFont(array(
                        'bold'       => true,
                        'italic'     => true,
                    ));
                });
                
                foreach ($_data as $d) {
                    $sheet->row($i, $d);
                    $sheet->row($i, function($row) use ($i) {
                        $row->setBackground($i % 2 == 0 ? '#FFFFFF' : '#e8e8e8');
                    });
                    
                    $i++;
                }
                
                $sheet->setAutoFilter('A7:H7');
                $sheet->setBorder('A7:H' . ($i - 1), 'thin');
                
                $i = 7;
                foreach ($_data as $d) {
                    for ($j = 0, $c = count($d); $j < count($d); $j++) { 
                        $sheet->cell(chr(65 + $j) . ($i + 1), function($cell) {
                            $cell->setBorder('thin', 'thick', 'thin', 'thick');
                        }); 
                    }
                    
                    $i++;
                }

                $sheet->setBorder('A7:H7', 'thick');

                $sheet->setPageMargin([
                    0.15, 0.15, 0.15, 0.15
                ]);

                $sheet->setWidth([
                    'A' => 11,
                    'B' => 45.5,
                    'C' => 26.5,
                    'D' => 53,
                    'E' => 10.5,
                    'F' => 14,
                    'G' => 9,
                    'H' => 17,
                ]);

                $sheet->setHeight([
                    7 => 30
                ]);
            });
        })->store('xlsx', storage_path('app/xlsx/Mp. Heres'));
    }

    public function csv($archivo, $callback)
    {
        $this->i = 0;
        if ($this->dev) {
            $this->info("Modo DEV");
        }
        
        $campos = [];
        DB::beginTransaction();
        if (($fichero = fopen($archivo, "r")) !== FALSE) {
            while (($datos = fgetcsv($fichero, 1000, ';')) !== FALSE) {
                if ($this->i === 0) {
                    $this->i++;
                    foreach ($datos as $campo) {
                        $campos[] = str_slug($campo, '_');
                    }
                    continue;
                }

                $linea = [];

                foreach ($campos as $i => $campo) {
                    //$linea[$i] = 
                    $linea[$campo] = $datos[$i];
                }

                $callback($linea);
                $this->i++;
            }

            fclose($fichero);
        }

        DB::commit();
    }
}
