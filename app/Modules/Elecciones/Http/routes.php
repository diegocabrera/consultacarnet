<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix'), 'namespace' => 'App\\Modules\Elecciones\Http\Controllers'], function()
{
   //Route::get('/', 'EleccionesController@index');


    Route::group(['prefix' => 'comunidades'], function() {
        Route::get('/',                 'ComunidadesController@index');
        Route::get('nuevo',             'ComunidadesController@nuevo');
        Route::get('cambiar/{id}',      'ComunidadesController@cambiar');
        
        Route::get('buscar/{id}',       'ComunidadesController@buscar');

        Route::post('guardar',          'ComunidadesController@guardar');
        Route::put('guardar/{id}',      'ComunidadesController@guardar');

        Route::delete('eliminar/{id}',  'ComunidadesController@eliminar');
        Route::post('restaurar/{id}',   'ComunidadesController@restaurar');
        Route::delete('destruir/{id}',  'ComunidadesController@destruir');

        Route::get('datatable',         'ComunidadesController@datatable');
    });


    Route::group(['prefix' => 'estatus_incidencia'], function() {
        Route::get('/',                 'EstatusIncidenciaController@index');
        Route::get('nuevo',             'EstatusIncidenciaController@nuevo');
        Route::get('cambiar/{id}',      'EstatusIncidenciaController@cambiar');
        
        Route::get('buscar/{id}',       'EstatusIncidenciaController@buscar');

        Route::post('guardar',          'EstatusIncidenciaController@guardar');
        Route::put('guardar/{id}',      'EstatusIncidenciaController@guardar');

        Route::delete('eliminar/{id}',  'EstatusIncidenciaController@eliminar');
        Route::post('restaurar/{id}',   'EstatusIncidenciaController@restaurar');
        Route::delete('destruir/{id}',  'EstatusIncidenciaController@destruir');

        Route::get('datatable',         'EstatusIncidenciaController@datatable');
    });


    Route::group(['prefix' => 'fuente_informacion'], function() {
        Route::get('/',                 'FuenteInformacionController@index');
        Route::get('nuevo',             'FuenteInformacionController@nuevo');
        Route::get('cambiar/{id}',      'FuenteInformacionController@cambiar');
        
        Route::get('buscar/{id}',       'FuenteInformacionController@buscar');

        Route::post('guardar',          'FuenteInformacionController@guardar');
        Route::put('guardar/{id}',      'FuenteInformacionController@guardar');

        Route::delete('eliminar/{id}',  'FuenteInformacionController@eliminar');
        Route::post('restaurar/{id}',   'FuenteInformacionController@restaurar');
        Route::delete('destruir/{id}',  'FuenteInformacionController@destruir');

        Route::get('datatable',         'FuenteInformacionController@datatable');
    });

     Route::group(['prefix' => 'incidencia'], function() {
        Route::get('/',                 'IncidenciaController@index');
        Route::get('nuevo',             'IncidenciaController@nuevo');
        Route::get('cambiar/{id}',      'IncidenciaController@cambiar');
        
        Route::get('buscar/{id}',       'IncidenciaController@buscar');

        Route::post('guardar',          'IncidenciaController@guardar');
        Route::put('guardar/{id}',      'IncidenciaController@guardar');

        Route::delete('eliminar/{id}',  'IncidenciaController@eliminar');
        Route::post('restaurar/{id}',   'IncidenciaController@restaurar');
        Route::delete('destruir/{id}',  'IncidenciaController@destruir');

        Route::get('datatable',         'IncidenciaController@datatable');
        
        Route::get('buscar_persona/{cedula}',         'IncidenciaController@buscarPersona');
        Route::get('parroquias/{id}',         'IncidenciaController@parroquias');
    });

    Route::group(['prefix' => 'carga'], function() {
        Route::get('/',                 'CargaController@index');
        Route::post('archivo',           'CargaController@archivo');
    });


    //{{route}}
});