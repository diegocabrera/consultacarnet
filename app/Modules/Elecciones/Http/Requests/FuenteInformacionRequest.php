<?php

namespace App\Modules\Elecciones\Http\Requests;

use App\Http\Requests\Request;

class FuenteInformacionRequest extends Request {
    protected $reglasArr = [
		'personas_id' => ['required', 'integer'], 
		'municipios_id' => ['integer'], 
		'parroquias_id' => ['integer'], 
		'comunidades_id' => ['integer'], 
		'nombre_calle' => ['min:3', 'max:200']
	];
}