<?php

namespace App\Modules\Elecciones\Http\Requests;

use App\Http\Requests\Request;

class UsuarioFuenteInformacionRequest extends Request {
    protected $reglasArr = [
		'app_usuario_id' => ['required', 'integer'], 
		'fuente_informacion_id' => ['required', 'integer']
	];
}