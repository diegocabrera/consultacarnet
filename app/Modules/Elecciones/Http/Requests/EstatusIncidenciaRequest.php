<?php

namespace App\Modules\Elecciones\Http\Requests;

use App\Http\Requests\Request;

class EstatusIncidenciaRequest extends Request {
    protected $reglasArr = [
		'descripcion' => ['required', 'min:3', 'max:100']
	];
}