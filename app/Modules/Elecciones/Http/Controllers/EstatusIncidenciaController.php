<?php

namespace App\Modules\Elecciones\Http\Controllers;

//Controlador Padre
use App\Modules\Elecciones\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Elecciones\Http\Requests\EstatusIncidenciaRequest;

//Modelos
use App\Modules\Elecciones\Models\EstatusIncidencia;

class EstatusIncidenciaController extends Controller
{
    protected $titulo = 'Estatus Incidencia';

    public $js = [
        'EstatusIncidencia'
    ];
    
    public $css = [
        'EstatusIncidencia'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('elecciones::EstatusIncidencia', [
            'EstatusIncidencia' => new EstatusIncidencia()
        ]);
    }

    public function nuevo()
    {
        $EstatusIncidencia = new EstatusIncidencia();
        return $this->view('elecciones::EstatusIncidencia', [
            'layouts' => 'base::layouts.popup',
            'EstatusIncidencia' => $EstatusIncidencia
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $EstatusIncidencia = EstatusIncidencia::find($id);
        return $this->view('elecciones::EstatusIncidencia', [
            'layouts' => 'base::layouts.popup',
            'EstatusIncidencia' => $EstatusIncidencia
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $EstatusIncidencia = EstatusIncidencia::withTrashed()->find($id);
        } else {
            $EstatusIncidencia = EstatusIncidencia::find($id);
        }

        if ($EstatusIncidencia) {
            return array_merge($EstatusIncidencia->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(EstatusIncidenciaRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $EstatusIncidencia = $id == 0 ? new EstatusIncidencia() : EstatusIncidencia::find($id);

            $EstatusIncidencia->fill($request->all());
            $EstatusIncidencia->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $EstatusIncidencia->id,
            'texto' => $EstatusIncidencia->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            EstatusIncidencia::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            EstatusIncidencia::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            EstatusIncidencia::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = EstatusIncidencia::select([
            'id', 'descripcion', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}