<?php

namespace App\Modules\Elecciones\Http\Controllers;

//Controlador Padre
use App\Modules\Elecciones\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use Modules\Elecciones\Http\Requests\ComunidadesRequest;

//Modelos
use App\Modules\Elecciones\Models\Comunidades;

class ComunidadesController extends Controller
{
    protected $titulo = 'Comunidades';

    public $js = [
        'Comunidades'
    ];
    
    public $css = [
        'Comunidades'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('elecciones::Comunidades', [
            'Comunidades' => new Comunidades()
        ]);
    }

    public function nuevo()
    {
        $Comunidades = new Comunidades();
        return $this->view('elecciones::Comunidades', [
            'layouts' => 'base::layouts.popup',
            'Comunidades' => $Comunidades
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Comunidades = Comunidades::find($id);
        return $this->view('elecciones::Comunidades', [
            'layouts' => 'base::layouts.popup',
            'Comunidades' => $Comunidades
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Comunidades = Comunidades::withTrashed()->find($id);
        } else {
            $Comunidades = Comunidades::find($id);
        }

        if ($Comunidades) {
            return array_merge($Comunidades->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ComunidadesRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Comunidades = $id == 0 ? new Comunidades() : Comunidades::find($id);

            $Comunidades->fill($request->all());
            $Comunidades->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Comunidades->id,
            'texto' => $Comunidades->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Comunidades::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Comunidades::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Comunidades::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Comunidades::select([
            'id', 'descripcion', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}