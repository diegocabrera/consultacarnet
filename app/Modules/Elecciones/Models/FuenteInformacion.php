<?php

namespace App\Modules\Elecciones\Models;

use App\Modules\Base\Models\Modelo;



class FuenteInformacion extends Modelo
{
    protected $table = 'fuente_informacion';
    protected $fillable = ["personas_id","municipios_id","parroquias_id","comunidades_id","nombre_calle"];
    protected $campos = [
    'personas_id' => [
        'type' => 'number',
        'label' => 'Personas',
        'placeholder' => 'Personas del Fuente Informacion'
    ],
    'municipios_id' => [
        'type' => 'select',
        'label' => 'Municipios',
        'placeholder' => 'Municipios del Fuente Informacion',
        'url' => 'municipios'
    ],
    'parroquias_id' => [
        'type' => 'select',
        'label' => 'Parroquias',
        'placeholder' => 'Parroquias del Fuente Informacion',
        'url' => 'parroquias'
    ],
    'comunidades_id' => [
        'type' => 'select',
        'label' => 'Comunidades',
        'placeholder' => 'Comunidades del Fuente Informacion',
        'url' => 'comunidades'
    ],
    'nombre_calle' => [
        'type' => 'text',
        'label' => 'Nombre Calle',
        'placeholder' => 'Nombre Calle del Fuente Informacion'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}