<?php

namespace App\Modules\Elecciones\Models;

use App\Modules\Base\Models\Modelo;



class EstatusIncidencia extends Modelo
{
    protected $table = 'estatus_incidencia';
    protected $fillable = ["descripcion"];
    protected $campos = [
    'descripcion' => [
        'type' => 'text',
        'label' => 'Descripcion',
        'placeholder' => 'Descripcion del Estatus Incidencia'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}