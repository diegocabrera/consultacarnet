@extends(isset($layouts) ? $layouts : 'base::layouts.default')

@section('content-top')
    @include('base::partials.botonera')
    
    @include('base::partials.ubicacion', ['ubicacion' => ['Comunidades']])
    
    @include('base::partials.modal-busqueda', [
        'titulo' => 'Buscar Comunidades.',
        'columnas' => [
            'Descripcion' => '100'
        ]
    ])
@endsection

@section('content')
    <div class="container">
        {!! Form::open(['id' => 'formulario', 'name' => 'formulario', 'method' => 'POST' ]) !!}
            {!! $Comunidades->generate() !!}
        {!! Form::close() !!}
    </div>
@endsection