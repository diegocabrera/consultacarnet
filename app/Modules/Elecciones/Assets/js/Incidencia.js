var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			//tabla.fnDraw();
			$('#datos-incidencia').hide();
			$("tbody", "#tablaTelefonos").html('');
			$('#nombre_persona').html('');
		},
		'buscar' : function(r){
			$('#datos-incidencia').show();
			$("tbody", "#tablaTelefonos").html('').append(tmpl('tmpl-telefonos', r));
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{"data":"fecha","name":"fecha"},
			{"data":"app_usuario_id","name":"app_usuario_id"},
			{"data":"municipios_id","name":"municipios_id"},
			{"data":"parroquias_id","name":"parroquias_id"},
			{"data":"estatus_incidencia_id","name":"estatus_incidencia_id"},
			{"data":"zona","name":"zona"},
			{"data":"descripcion","name":"descripcion"},{"data":"accion_tomada","name":"accion_tomada"}
		]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});

	$('#municipios_id').change(function(){
		aplicacion.selectCascada($(this).val(), 'parroquias_id','parroquias');
		$('#parroquias_id option[value=""]').text('Todas las Parroquias');
	});

	$("#agregar").on('click', function(){
		$("tbody", "#tablaTelefonos").append(tmpl('tmpl-telefonos', {
			telefono : [{
				id: '',
				numero : '',
			}]
		}));
	});

	$("tbody", "#tablaTelefonos").on('click', '.btn', function(){
		$(this).parents('tr').remove();
	});

	$('#fecha', $form).datetimepicker();
	$('#fecha_accion_tomada', $form).datetimepicker();
	$('#fecha_estatus', $form).datetimepicker();

	$('#datos-incidencia').hide();
	

	$('#estatus_incidencia_id').change(function(){
		var estatus = $(this).val();
		if(estatus == 1 || estatus == ''){
			$('#datos-incidencia').hide();
		}else{
			$('#datos-incidencia').show();
		}
	});

	$('#cedula').blur(function(){
		var cedula = $(this).val();
		if(cedula != 0){
			buscar_persona(cedula);
		}
	});
});

function buscar_persona(cedula){
	$.ajax({
		url: $url + 'buscar_persona/' + cedula,
		type: 'GET',
		success: function(r){
			aviso(r.msj);
			if(r.asignado){
				aviso(r.asignado.msj);
				
			}
			if(r.s == 's' && r.asignado.s == 's'){

				$('#nombre_persona').html(r.persona.nombres);
				$('#persona_contacto_id').val(r.persona.id);
				$("tbody", "#tablaTelefonos").html('').append(tmpl('tmpl-telefonos', r));

				//aplicacion.rellenar(r);
			}
		}
	});
}


/*var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.ajax.reload();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{"data":"fecha","name":"fecha"},
			{"data":"app_usuario_id","name":"app_usuario_id"},
			{"data":"municipios_id","name":"municipios_id"},
			{"data":"parroquias_id","name":"parroquias_id"},
			{"data":"estatus_incidencia_id","name":"estatus_incidencia_id"},
			{"data":"zona","name":"zona"},
			{"data":"descripcion","name":"descripcion"},{"data":"accion_tomada","name":"accion_tomada"}
		]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});
});*/