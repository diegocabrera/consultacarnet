var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.ajax.reload();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [{"data":"personas_id","name":"personas_id"},{"data":"municipios_id","name":"municipios_id"},{"data":"parroquias_id","name":"parroquias_id"},{"data":"comunidades_id","name":"comunidades_id"},{"data":"nombre_calle","name":"nombre_calle"}]
	});
	
	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});
});