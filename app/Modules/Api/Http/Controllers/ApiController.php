<?php
namespace App\Modules\Api\Http\Controllers;


use Illuminate\Http\Response;
use App\Modules\Api\Http\Controllers\Controller;
use DB;
//use App\Http\Requests\Request;
//use Illuminate\Http\Response;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Excel;
use Illuminate\Http\Request;

use App\Modules\Consultapsuv\Models\Centros;
use App\Modules\Consultapsuv\Models\Elecciones;
use App\Modules\Consultapsuv\Models\ElecionesResultados;
use App\Modules\Consultapsuv\Models\RepBolivar;
use App\Modules\Consultapsuv\Models\Rep;
use App\Modules\Consultapsuv\Models\FirmantesRev;
use App\Modules\Consultapsuv\Models\PersonasAyudas;
use App\Modules\Consultapsuv\Models\Ayudas;
use App\Modules\Consultapsuv\Models\HistorialAyudas;
use App\Modules\Consultapsuv\Models\RequerimientosAyudas;
use App\Modules\Consultapsuv\Models\ResponsablesAyudas;


class ApiController extends Controller {
	//public $autenticar = false;

	//protected $titulo = 'Escritorio';
	public $librerias = [
		'highcharts',
		'highcharts-drilldown',
        'highcharts-3d',
        'datatables',
	];
	public $js =[];
	public $css =[];
    public $data = []; //adonde voy almacenar el dato a buscar en el data table
	// public function __construct() {
	// 	parent::__construct();
	// 	$this->middleware('auth');
	// }


	public function grafica(Request $request){
        //graficas
		$adonde = $request->tipo;
        //$adonde = 1;
        //dd($request->all());
		switch ($adonde) {
			case 1:
				$result = 'P';
				$text = 'Presidenciales';
				break;
			case 2:
				$result = 'R';
				$text = 'Regionales';
				break;
			case 3:
				$result = 'M';
				$text = 'Municipales y Parroquiales';
				break;
			case 4:
				$result  = 'O';
				$result2 = 'REFERENDUM CONSTITUCIONAL B';
				$text = 'otras';
				break;
			case 5:
				$result  = 6;
				$result2 = 'Ultimas 5 Elecciones';
				$text = 'Ultimas 5 Elecciones';
				break;
        }

		$datos = ElecionesResultados::select([
			//'ElecionesResultados2.descrip_eleccion',
			'elecciones.nombre',
			'elecciones.fecha',
			DB::raw('Sum(eleciones_resultados.oficialismo) as votosoficiales'),
			DB::raw('Sum(eleciones_resultados.oposicion) as votosoposicion'),

		])
		->leftJoin('elecciones', 'elecciones.id','=', 'eleciones_resultados.elecciones_id')
		->leftJoin('centros', 'eleciones_resultados.centros_id','=', 'centros.id')
		//->groupby('ElecionesResultados.descrip_eleccion')
		->groupby('elecciones.fecha')
		->groupby('elecciones.nombre')
        ->orderBy('elecciones.fecha');

        //dd($datos->toSql());



        if( $result != 6){
            $datos->where('elecciones.tipo', $result)
            ->orderBy('elecciones.nombre', 'asc');
        } else {
            $elecciones = $datos->get();
            $datos->limit(6)
                ->offset(count($elecciones) - 6);
        }

		if($request->adonde == 'municipio'){
			$datos->where('centros.municipio', $request->nombre);
		}
		if($request->adonde == 'parroquia'){
			$datos->where('centros.parroquia', $request->nombre);
		}

		$categorias = [];
		$series['psuv'] = [
            'name' => 'Psuv'
        ];
		$series['oposicion'] = [
            'name' => 'Oposicion'
        ];

		foreach ($datos->take(6)->get()  as $key => $value) {
			$categorias[]= $value->nombre;
			$series['psuv']['data'][] = intval($value->votosoficiales);
			$series['oposicion']['data'][] = intval($value->votosoposicion) ;

		}

		$series = array_values($series);

		return[
            'categories' =>  $categorias,
            'series'     =>  $series
        ];
	}

	public function graficatorta(Request $request){

		//return 'hola';
		$adonde = $request->tipo;
		//$adonde = 1;
		switch ($adonde) {
			case 1:
				$result = 'P';
				$text = 'Presidenciales';
				break;
			case 2:
				$result = 'R';
				$text = 'Regionales';
				break;
			case 3:
				$result = 'M';
				$text = 'Municipales y Parroquiales';
				break;
			case 4:
				$result  = 'O';
				$result2 = 'REFERENDUM CONSTITUCIONAL B';
				$text = 'otras';
                break;
            case 5:
				$result  = 6;
				$result2 = 'Ultimas 5 Elecciones';
				$text = 'Ultimas 5 Elecciones';
				break;
		}


		$datos = ElecionesResultados::select([
			//'ElecionesResultados2.descrip_eleccion',
			'elecciones.nombre',
			'elecciones.fecha',
			DB::raw('Sum(eleciones_resultados.abstencion) as abstencion'),
			DB::raw('Sum(eleciones_resultados.oficialismo) as votosoficiales'),
			DB::raw('Sum(eleciones_resultados.oposicion) as votosoposicion'),
		])
		->leftJoin('elecciones', 'elecciones.id','=', 'eleciones_resultados.elecciones_id')
		->leftJoin('centros', 'eleciones_resultados.centros_id','=', 'centros.id')
		->groupby('elecciones.fecha')
		->groupby('elecciones.nombre')
        ->orderBy('elecciones.fecha');

        if( $result != 6){
            $datos->where('elecciones.tipo', $result)
            ->orderBy('elecciones.nombre', 'asc');
        }else {
            $elecciones = $datos->get();
            $datos->limit(6)
                ->offset(count($elecciones) - 6);
        }

		if($request->adonde == 'municipio'){
			$datos->where('centros.municipio', $request->nombre);
		}
		if($request->adonde == 'parroquia'){
			$datos->where('centros.parroquia', $request->nombre);
		}

		$drilldown = [];

		$series['prueba'] = [
			'name' => $text,
			'colorByPoint'=> true,
		];


		foreach ($datos->take(6)->get() as $key => $value) {
			$series['prueba']['data'][] = [
				'name' => $value->nombre,
				'y'	   => $value->votosoficiales + $value->abstencion + $value->votosoposicion,
				'drilldown' => str_slug($value->nombre, '_'),
			];
			//drilldown
			$drilldown['series'][] = [
				'name' => $value->nombre,
				'id' => str_slug($value->nombre, '_'),
				'data' => [
					['Psuv', intval($value->votosoficiales)],
					['Oposicion', intval($value->votosoposicion)],
					['Abstencion', intval($value->abstencion)]
				]
			];
		}


		$series = array_values($series);
		//$drilldown = array_values($drilldown);

		return[
            'drilldown' =>  $drilldown,
            'series'     =>  $series
        ];

	}

	public function tablacomparacion(Request $request){

		$_datos = ElecionesResultados::select([
			'elecciones.nombre',
			'centros.municipio',
			DB::raw('Sum(eleciones_resultados.oficialismo) as votosoficiales'),
			DB::raw('Sum(eleciones_resultados.oposicion) as votosoposicion'),
		])
		->leftJoin('elecciones', 'elecciones.id','=', 'eleciones_resultados.elecciones_id')
		->leftJoin('centros', 'eleciones_resultados.centros_id','=', 'centros.id')
		->whereIn('elecciones.nombre', [
			'Regionales 2008',
			'Elecciones Parlamentarias 2010',
			'Presidenciales 2012',
			'Regionales 2012',
			'Presidenciales 2013',
            'Elecciones Parlamentarias 2015',
            'Elecciones Regionales 2017'

		])
		->where('centros.municipio','!=', '')
		->groupby('elecciones.nombre', 'centros.municipio')
		->orderBy('centros.municipio', 'asc')
		->orderBy('elecciones.fecha', 'asc');
		//->where('datos_centros.municipio_centro', 'MP. HERES')->get(); */
		;

		//dd($_datos->toSql());

		$datos =[];
		$resultado = 0;
		foreach ($_datos->get() as $value) {
			$resultado = 0;
			if($value->votosoficiales > $value->votosoposicion){
				$votosoficiales = strval($value->votosoficiales)."(G)";
				$resultado = $value->votosoficiales - $value->votosoposicion;
			}else{
				$votosoficiales = strval($value->votosoficiales)."(P)";
				$resultado = $value->votosoficiales - $value->votosoposicion;
			}

			$datos[$value->municipio]['municipios'] = $value->municipio;
			$datos[$value->municipio][$value->nombre] = [
				'votosoficiales' => $votosoficiales,
				'votosoposicion' => $value->votosoposicion,
				'resultado'		 => $resultado
			];
		}

       $poblacion = DB::table('poblacion')
		->select([
           	DB::raw('poblacion'),
		])
        ->where('nombre','bolivar')->first();


        $electores = Rep::select([
			DB::raw('Count(id) as electores'),
		])
		->first();

		$militantes = Rep::select([
			DB::raw('Count(id) as militante'),
		])
		->where('psuv', 1)
		->first();

		$centros = Centros::select([
			DB::raw('Count(id) as centros'),
			DB::raw('Sum(mesas) as mesas'),
		])
		->where('activo', 1)
		->first();

		$parroquias = Centros::select([
			'parroquia',
		])
		->groupby('parroquia')
		->where('activo', 1)
		->where('parroquia', '!=' , '')
		->get();

		$num = 0;
		 foreach ($parroquias as $value) {
		 	$num++;
		 }

		return[
            'datos'     => $datos,
            'poblacion' => $poblacion->poblacion,
            'electores' => $electores->electores,
            'militante' => $militantes->militante,
            'centros'   => $centros->centros,
            'mesas' 	=> $centros->mesas,
            'parroquias'=> $num
        ] ;
		//return $datos;
	}

	public function comparacionparroquias(Request $request){
		$_datos = ElecionesResultados::select([
			'elecciones.nombre',
			'centros.parroquia as municipio',
			DB::raw('Sum(eleciones_resultados.oficialismo) as votosoficiales'),
			DB::raw('Sum(eleciones_resultados.oposicion) as votosoposicion'),

		])
		->leftJoin('elecciones', 'elecciones.id','=', 'eleciones_resultados.elecciones_id')
		->leftJoin('centros', 'eleciones_resultados.centros_id','=', 'centros.id')
		->whereIn('elecciones.nombre', [
			"Regionales 2008",
			"Elecciones Parlamentarias 2010",
			"Presidenciales 2012",
			"Regionales 2012",
			"Presidenciales 2013",
			"Elecciones Parlamentarias 2015",
			'Elecciones Regionales 2017'
		])
		->where('centros.parroquia','!=', '')
		->orderBy('centros.parroquia', 'asc')
		->groupby('elecciones.nombre')
		->groupby('centros.parroquia')
		->orderBy('elecciones.fecha', 'asc')
		->where('centros.municipio', $request->tipo);


		$datos =[];
		$resultado = 0;
		foreach ($_datos->get() as $value) {
			$resultado = 0;
			if($value->votosoficiales > $value->votosoposicion){
				$votosoficiales = strval($value->votosoficiales)."(G)";
				$resultado = $value->votosoficiales - $value->votosoposicion;
			}else{
				$votosoficiales = strval($value->votosoficiales)."(P)";
				$resultado = $value->votosoficiales - $value->votosoposicion;
			}

			$datos[$value->municipio]['municipios'] = $value->municipio;
			$datos[$value->municipio][$value->nombre] = [
				'votosoficiales' => $votosoficiales,
				'votosoposicion' => $value->votosoposicion,
				'resultado'		 => $resultado
			];
		}


		$poblacion = $patria_carnet = DB::table('poblacion')
		->select([
           	DB::raw('poblacion'),
		])
        ->where('nombre',$request->tipo)->first();


        $electores = Rep::select([
			DB::raw('Count(rep.id) as electores'),

		])
		->leftJoin('centros', 'rep.centro_id','=', 'centros.id')
		->where('centros.municipio',$request->tipo)
		->first();

		$militantes = Rep::select([
			DB::raw('Count(rep.id) as militante'),
		])
		->leftJoin('centros', 'rep.centro_id','=', 'centros.id')
		->where('centros.municipio',$request->tipo)
		->where('psuv', 1)
		->first();

		$centros = Centros::select([
			DB::raw('Count(id) as centros'),
			DB::raw('Sum(mesas) as mesas'),
		])
		->where('municipio',$request->tipo)
		->where('activo', 1)
		->first();

		$parroquias = Centros::select([
			'parroquia',
		])
		->groupby('parroquia')
		->where('activo', 1)
		->where('centros.municipio',$request->tipo)
		->where('parroquia', '!=' , '')
		->get();

		$num = 0;
		 foreach ($parroquias as $value) {
		 	$num++;
		 }

		return[
            'datos'     => $datos,
            'poblacion' => $poblacion->poblacion,
            'electores' => $electores->electores,
            'militante' => $militantes->militante,
            'centros'   => $centros->centros,
            'mesas' 	=> $centros->mesas,
            'parroquias'=> $num
        ];

	}

	public function comparacioncentros(Request $request){
		$_datos = ElecionesResultados::select([
			'elecciones.nombre',
			'centros.nombre as municipio',
			'centros.id',
			DB::raw('Sum(eleciones_resultados.oficialismo) as votosoficiales'),
			DB::raw('Sum(eleciones_resultados.oposicion) as votosoposicion'),

		])
		->leftJoin('elecciones', 'elecciones.id','=', 'eleciones_resultados.elecciones_id')
		->leftJoin('centros', 'eleciones_resultados.centros_id','=', 'centros.id')
		->whereIn('elecciones.nombre', [
			"Regionales 2008",
			"Elecciones Parlamentarias 2010",
			"Presidenciales 2012",
			"Regionales 2012",
			"Presidenciales 2013",
			"Elecciones Parlamentarias 2015",
			'Elecciones Regionales 2017'
		])
		->where('centros.nombre','!=', '')
		->orderBy('centros.nombre', 'asc')
		->groupby('elecciones.nombre')
		->groupby('centros.id')
		->groupby('centros.nombre')
		->orderBy('elecciones.fecha', 'asc')
		->where('centros.parroquia', $request->tipo);


		$datos =[];
		$resultado = 0;
		foreach ($_datos->get() as $value) {
			$resultado = 0;
			if($value->votosoficiales > $value->votosoposicion){
				$votosoficiales = strval($value->votosoficiales)."(G)";
				$resultado = $value->votosoficiales - $value->votosoposicion;
			}else{
				$votosoficiales = strval($value->votosoficiales)."(P)";
				$resultado = $value->votosoficiales - $value->votosoposicion;
			}

			$datos[$value->municipio]['municipios'] = $value->municipio;
			$datos[$value->municipio]['id'] = $value->id;
			$datos[$value->municipio][$value->nombre] = [
				'votosoficiales' => $votosoficiales,
				'votosoposicion' => $value->votosoposicion,
				'resultado'		 => $resultado
			];
		}

	   $poblacion = $patria_carnet = DB::table('poblacion')
		->select([
           	DB::raw('poblacion'),
		])
        ->where('nombre',$request->tipo)->first();


        $electores = Rep::select([
			DB::raw('Count(rep.id) as electores'),

		])
		->leftJoin('centros', 'rep.centro_id','=', 'centros.id')
		->where('centros.parroquia',$request->tipo)
		->first();

		$militantes = Rep::select([
			DB::raw('Count(rep.id) as militante'),
		])
		->leftJoin('centros', 'rep.centro_id','=', 'centros.id')
		->where('centros.parroquia',$request->tipo)
		->where('psuv', 1)
		->first();

		$centros = Centros::select([
			DB::raw('Count(id) as centros'),
			DB::raw('Sum(mesas) as mesas'),
		])
		->where('parroquia',$request->tipo)
		->where('activo', 1)
		->first();

		$parroquias = Centros::select([
			'parroquia',
		])
		->groupby('parroquia')
		->where('activo', 1)
		->where('centros.parroquia',$request->tipo)
		->where('parroquia', '!=' , '')
		->get();

		$num = 0;
		 foreach ($parroquias as $value) {
		 	$num++;
		 }

		return[
            'datos'     => $datos,
            'poblacion' => $poblacion->poblacion,
            'electores' => $electores->electores,
            'militante' => $militantes->militante,
            'centros'   => $centros->centros,
            'mesas' 	=> $centros->mesas,
            'parroquias'=> $num
        ];

	}

	public function consulta(Request $request){
		//dd('holaaa');

         $this->js = [
             'main',
         ];
         $this->css = [
             'main',
		 ];
		/* informacion personal */
    	$informacion = $this->rep($request->id);

    		 return $this->view('consultapsuv::Consulta', [
    			'ci'				    		=> $request->id,
    		 	'informacion_personal'  		=> $informacion,
    		 	'rep_psuv2010' 					=> $this->rep_psuv($request->id),
    		 	'info_centro'					=> $this->info_centro($informacion->centro_id),
    		 	'inscritos'						=> $this->inscritos($request->id),
    		 	'psa_bolivar'					=> $this->psa_bolivar($request->id),
    		 	'hogares_patria'				=> $this->hogares_patria($request->id),
                 'ccc2012'               		=> $this->ccc2012($request->id),
                 'ccc_municipales_2013'  		=> $this->ccc_municipales_2013($request->id),
                 'ccc_parroquiales_2013' 		=> $this->ccc_parroquiales_2013($request->id),
                 'anillo_2010'           		=> $this->anillo_2010($request->id),
    		 	'ubc_resp1x10'          		=> $this->ubc_resp1x10($request->id),
    		 	'pat_territoriales_tmunicipios' => $this->pat_territoriales_tmunicipios($request->id),
    		 	'pat_sectoriales_municipios'    => $this->pat_sectoriales_municipios($request->id),
    		 	'postulados'                    => $this->postulados($request->id),
    		 	'actores_electorales'           => $this->actores_electorales($request->id),
    		 	'varios_votos'           		=> $this->varios_votos($request->id),
    		 	'patria_carnet'           		=> $this->patria_carnet($request->id),
    		 ]);
	}

    public function consulta_api (Request $request)
	{
		$this->autenticar = false;
		
		/* informacion personal */
		$informacion = $this->rep($request->id);
		//exit(json_encode($informacion));
		if(!$informacion){
			return response()->json(['s'=> 'n']);
		}
		$consulta = [
			's'								=> 's',
			'ci'				    		=> $request->id,
		 	'informacion_personal'  		=> $informacion,
		 	'rep_psuv2010' 					=> $this->rep_psuv($request->id),
		 	'info_centro'					=> $this->info_centro($informacion->centro_id),
		 	'inscritos'						=> $this->inscritos($request->id),
		 	'psa_bolivar'					=> $this->psa_bolivar($request->id),
		 	'hogares_patria'				=> $this->hogares_patria($request->id),
            'ccc2012'               		=> $this->ccc2012($request->id),
            'ccc_municipales_2013'  		=> $this->ccc_municipales_2013($request->id),
            'ccc_parroquiales_2013' 		=> $this->ccc_parroquiales_2013($request->id),
            'anillo_2010'           		=> $this->anillo_2010($request->id),
		 	'ubc_resp1x10'          		=> $this->ubc_resp1x10($request->id),
		 	'pat_territoriales_tmunicipios' => $this->pat_territoriales_tmunicipios($request->id),
		 	'pat_sectoriales_municipios'    => $this->pat_sectoriales_municipios($request->id),
		 	'postulados'                    => $this->postulados($request->id),
		 	'actores_electorales'           => $this->actores_electorales($request->id),
		 	'varios_votos'           		=> $this->varios_votos($request->id),
		 	'patria_carnet'           		=> $this->patria_carnet($request->id),
			'ayudas'						=> $this->ayudas($request->id),
		];

		return response()->json($consulta);
	}

	public function ayudas($dni)
	{
		//6647489
		try {
			$personas_ayudas = PersonasAyudas::select(['id','cedula', 'nombres'])->where('cedula', $dni)->first();
				$ayudas = Ayudas::select([
					'id',
					'personas_ayudas_id',
					'resumen',
					'tipo_ayuda_id',
					'fecha_creacion',
				])->where('personas_ayudas_id', $personas_ayudas->id)->orderBy('id', 'desc')->first();
				$tipo_ayuda = RequerimientosAyudas::select()->where('id', $ayudas->tipo_ayuda_id)->first();
				$tipo_ayuda_respon = explode(',', $tipo_ayuda->responsables);
				$historia_ayuda = HistorialAyudas::select(['id', 'ayudas_id', 'observacion'])
																	->where('ayudas_id', $ayudas->id)
																		->orderBy('id', 'desc')
																			->first();
				$cuenta_ayudas = Ayudas::select('personas_ayudas_id')->where('personas_ayudas_id', $personas_ayudas->id)->groupBy('personas_ayudas_id')->count();


				$rs = [
					'nombre'				=> 	$personas_ayudas->nombres,
					'cedula'				=> 	$personas_ayudas->cedula,
					'ultima_ayuda'			=>	$ayudas->resumen,
					'ultima_observacion'	=>	$historia_ayuda->observacion,
					'ultima_ayuda_tipo'		=>	$tipo_ayuda->nombre,
					'ayudas_solicitadas'	=>	$cuenta_ayudas,
				];
		} catch (\Exception $e) {
			$rs = '';
		}

		//dd($ayudas);
		return $rs;
	}

    public function patria_carnet($dni)
	{
		$patria_carnet = DB::table('s_v_carnet_patria')
		->select([
            'cedula',
            'parentesco',
            'telefonos',
            'ambito_especifico',
            'sub_ambito',
            'estatus',
            'codigo_centro',
            'clap',
            'clap_lc',
            'fecha_registro',
		])
		->where('cedula', $dni)->first();
		if(count($patria_carnet)== 0){
			$patria_carnet = (object) [
                'cedula'                => '',
                'parentesco'            => '',
                'telefonos'             => '',
                'ambito_especifico'     => '',
                'sub_ambito'            => '',
                'estatus'               => '',
                'codigo_centro'         => '',
                'clap'                  => '',
                'clap_lc'               => '',
                'fecha_registro'        => '',
			];
		}
		return $patria_carnet;
	}

	public function varios_votos($dni){
		$varios_votos = DB::table('varios_votos')
		->select([
            "v7oct",
            "v14abr",
            "v28jul",
            "v6d",
            "primud",
            "firmo",
            "firma_maduro",
            "opoc",
            "ubch",
            "trabajo",
            "cargo",
            "nomina",
            "unidad_adscripcion",
            "secretaria_adscripcion",
		])
		->where('cedula', $dni)->first();
		if(count($varios_votos)== 0){
			$varios_votos = (object) [
				"v7oct"						=> '',
	            "v14abr"					=> '',
	            "v28jul"					=> '',
	            "v6d"						=> '',
	            "primud"					=> '',
	            "firmo"						=> '',
	            "firma_maduro"				=> '',
	            "ubch"						=> '',
			];
		}
		return $varios_votos;
	}

	public function actores_electorales($dni){
		$actores_electorales = DB::table('actores_electorales')
		->select([
            "cargo_electoral",
		])
		->where('cedula_actor_electoral', $dni)
		->where('cargo_electoral', '"Buro Politico PSUV"')->first();
		if(count($actores_electorales)== 0){
			$actores_electorales = (object) [
                "cargo_electoral"	=> '',
			];
		}
		return $actores_electorales;
	}

	public function postulados($dni){
		$postulados = DB::table('postulados')
		->select([
            "cedula_postulado_varios",
            "delegado_cong_fund",
            "jefe_batallon",
            "postulado_dip_an",
            "post_cand_cleb",
            "post_delegado_extra",
		])
		->where('cedula_postulado_varios', $dni)->first();
		if(count($postulados)== 0){
			$postulados = (object) [
                "cedula_postulado_varios"   => '',
                "delegado_cong_fund"        => '',
                "jefe_batallon"             => '',
                "postulado_dip_an"          => '',
                "post_cand_cleb"            => '',
                "post_delegado_extra"       => '',
			];
		}
		return $postulados;
	}

	public function pat_sectoriales_municipios($dni){
		$pat_sectoriales_municipios = DB::table('pat_sectoriales_municipios')
		->select([
			"cedula_patrulla_sect",
			"codigo_pat_sectorial",
			"cedula_responsable_sect"
		])
		->where('cedula_patrulla_sect', $dni)->first();
		if(count($pat_sectoriales_municipios)== 0){
			$pat_sectoriales_municipios = (object) [
				"cedula_patrulla_sect" => '',
				"codigo_pat_sectorial" => '',
				"cedula_responsable_sect" => '',
			];
		}
		return $pat_sectoriales_municipios;
	}

	public function pat_territoriales_tmunicipios($dni){
		$pat_territoriales_tmunicipios = DB::table('pat_territoriales_tmunicipios')
		->select([
			"ced_resp",
			"ced_patrulla",
			"codigo_pat_territorial"
		])
		->where('ced_patrulla', $dni)->first();
		if(count($pat_territoriales_tmunicipios)== 0){
			$pat_territoriales_tmunicipios = (object) [
				"ced_resp" => '',
				"ced_patrulla" => '',
				"codigo_pat_territorial" => ''
			];
		}
		return $pat_territoriales_tmunicipios;
	}

	public  function dataterisect(Request $request){

        $sql = DB::table('pat_sectoriales_municipios')
		->select([
			'cedula_patrulla_sect',
			'codigo_pat_sectorial',
			'MUNICIPIO',
			'cedula_sectorial',
			'NOMBRES',
			'APELLIDOS',
			'TELEFONO_RESIDENCIA',
			'MUNICIPIO_NOMBRE',
			'PARROQUIA_NOMBRE',
			'GEOGRAFICO_DES',
			'SECTOR',
			'URBANIZACION'
		])
        ->where('codigo_pat_sectorial', $request->ci);

        return Datatables::of($sql)
                ->setRowId('cedula_patrulla_sect')
                ->make(true);
	}

	public  function dataterimunicipio(Request $request){

        $sql = DB::table('pat_territoriales_tmunicipios')
		->select([

			'pat_territoriales_tmunicipios.des_municipio',
			'pat_territoriales_tmunicipios.ced_patrulla',
			'pat_territoriales_tmunicipios.desp_parroquia',
			'rep_bolivar.nombre_rep'

		])
		->leftJoin('rep_bolivar', 'rep_bolivar.cedula','=', 'pat_territoriales_tmunicipios.ced_patrulla')
        ->where('pat_territoriales_tmunicipios.codigo_pat_territorial', $request->ci);


        return Datatables::of($sql)
                ->setRowId('ced_patrulla')
                ->make(true);
	}

	public  function personascentro(Request $request){

        $sql = DB::table('miembro1x10')
		->select([
			"miembro1x10.cedula_mi1x10",
            "miembro1x10.nombres_mi1x10",
            "miembro1x10.apellidos_mi1x10",
            "miembro1x10.centro_mi1x10",
            "miembro1x10.telefono_mi1x10",
            DB::raw("(select nomcentro from datos_centros where codcentro = miembro1x10.centro_mi1x10 limit 1) as centro")
        ])
        ->where('miembro1x10.cedula_resp', $request->ci);

        return Datatables::of($sql)
                ->setRowId('cedula_mi1x10')
                ->make(true);
    }

	public function ubc_resp1x10($dni){
		$ubc_resp1x10 = DB::table('ubc_resp1x10')
		->select([
			"codigo_centro_ubch_1x10",
		])
		->where('cedula', 'v-'.$dni)->first();
		if(count($ubc_resp1x10)== 0){

			$ubc_resp1x10 = (object) [
				"codigo_centro_ubch_1x10" => '',
			];
		}
		return $ubc_resp1x10;
    }

	public function anillo_2010($dni){
		$anillo_2010 = DB::table('anillo_2010')
		->select([
			"anillo",
		])
		->where('cedula', $dni)->first();
		if(count($anillo_2010)== 0){

			$anillo_2010 = (object) [
				"anillo" => '',
			];
		}
		return $anillo_2010;
    }

	public function ccc_parroquiales_2013($dni){
		$ccc_parroquiales_2013 = DB::table('ccc_parroquiales_2013')
		->select([
			"cargo_ccp",
		])
		->where('cedula', $dni)->first();
		if(count($ccc_parroquiales_2013)== 0){

			$ccc_parroquiales_2013 = (object) [
				"cargo_ccp" => '',
			];
		}
		return $ccc_parroquiales_2013;
    }

	public function ccc_municipales_2013($dni){
		$ccc_municipales_2013 = DB::table('ccc_municipales_2013')
		->select([
			"cargo_ccm",
		])
		->where('cedula', $dni)->first();
		if(count($ccc_municipales_2013)== 0){

			$ccc_municipales_2013 = (object) [
				"cargo_ccm" => '',
			];
		}
		return $ccc_municipales_2013;
    }

	public function ccc2012($dni){
		$ccc2012 = DB::table('ccc2012')
		->select([
			"cargo_cce",
		])
		->where('cedula', $dni)->first();
		if(count($ccc2012)== 0){

			$ccc2012 = (object) [
				"cargo_cce" => '',
			];
		}
		return $ccc2012;
	}

	public function hogares_patria($dni){
		$hogares_patria = DB::table('hogares_patria')
		->select([
			"direccion_hp",
		])
		->where('cedula', $dni)->first();
		if(count($hogares_patria)== 0){

			$hogares_patria = (object) [
				"direccion_hp" => '',
			];
		}
		return $hogares_patria;
	}

	public function info_centro($centro_id){
		$info_centro = Centros::where('id', $centro_id)->first();

		$electores = Rep::select([
			DB::raw('Count(id) as electores'),
		])
		->where('centro_id', $centro_id)
		->first();

		$militantes = Rep::select([
			DB::raw('Count(id) as militante'),
		])
		->where('centro_id', $centro_id)
		->where('psuv', 1)
		->first();

		$info_centro->electores  = $electores->electores;
		$info_centro->militantes = $militantes->militante;

		return $info_centro;
	}

	public function rep($dni){
		$informacion = Rep::where('dni', $dni)->first();
		if(!$informacion){
			return $informacion;
		}
		$informacion->edad = Carbon::parse($informacion->nacimiento)->age;;
		$informacion->nacimiento =  Carbon::parse($informacion->nacimiento)->format('d/m/Y');
		return $informacion;
	}

	public function rep_psuv($dni){
		$rep_psuv = DB::table('rep_psuv2010f')
		->select([
			"municipio_nombre",
			"parroquia_nombre",
			"direccion",
		])
		->where('cedula', $dni)->first();
		if(count($rep_psuv)== 0){

			$rep_psuv = (object) [
				"municipio_nombre" => '',
				"parroquia_nombre"=> '',
				"direccion"=> '',
			];
		}
		return $rep_psuv;
	}

	public function psa_bolivar($dni){
		$psa_bolivar = DB::table('psa_bolivar')
		->select([
			"telefono_habitacion",
			"telefono_movil",
			"telefono_oficina",
			"correo_electronico",
			"twitter",
		])
		->where('cedula', $dni)->first();
		if(count($psa_bolivar)== 0){

			$psa_bolivar = (object) [
				"telefono_habitacion" => '',
				"telefono_movil" => '',
				"telefono_oficina" => '',
				"correo_electronico" => '',
				"twitter" => '',
			];
		}
		return $psa_bolivar;
	}

	public function inscritos($dni){

		$inscritos = DB::table('tbl_inscrito')
		->where('cedula',$dni)->first();

		if(count($inscritos)== 0){

			$inscritos = (object) [
					"inscrito_2007"=> "NO",
					"inscrito_2009"=> "No",
					"inscrito_2010"=> "No",
					"inscrito_psuv"=> "NO",
					"actualizado_2009"=> "NO"
				];
			}

		return $inscritos;
	}

	public function municipios(){
		$municipios = DatosCentros::select('municipio_centro')
		->groupby('municipio_centro')
		->pluck('municipio_centro', 'municipio_centro');
		return $municipios;
	}

	public function centros(Request $request){

        $info_centro = Centros::where('id', $request->centro)->get();

        $mesas = $this->mesas_electorales($request->centro);
       	$militantes = $this->militantes($request->centro);
        $datos = $this->tabla_centro($request->centro);

        $carnet = $patria_carnet = DB::table('s_v_carnet_patria')
		->select([
           	DB::raw('count(id) as total'),
		])
        ->where('codigo_centro', $request->centro)->first();

		$grafica = ElecionesResultados::select([
			//'ElecionesResultados2.descrip_eleccion',
			'elecciones.nombre',
			'elecciones.fecha',
			DB::raw('Sum(eleciones_resultados.oficialismo) as votosoficiales'),
			DB::raw('Sum(eleciones_resultados.oposicion) as votosoposicion'),

		])
		->leftJoin('elecciones', 'elecciones.id','=', 'eleciones_resultados.elecciones_id')
		->leftJoin('centros', 'eleciones_resultados.centros_id','=', 'centros.id')
		//->groupby('ElecionesResultados.descrip_eleccion')
		->groupby('elecciones.fecha')
		->groupby('elecciones.nombre')
		->groupby('elecciones.tipo')
        //->orderBy('elecciones.nombre', 'asc')
        ->orderBy('elecciones.fecha', 'asc')
		->where('centros.id', $request->centro);



		$categorias = [];
		$series['psuv'] = [
            'name' => 'Psuv'
        ];
		$series['oposicion'] = [
            'name' => 'Oposicion'
        ];

		foreach ($grafica->get() as $key => $value) {
			$categorias[]= $value->nombre;
			$series['psuv']['data'][] = intval($value->votosoficiales);
			$series['oposicion']['data'][] = intval($value->votosoposicion);
		}

        $series = array_values($series);

        $firmantes = DB::connection('bolivar_psuv')
        ->table('rep_psuv2010f')
        ->select([
            DB::raw('Count(centro) as total')
        ])

        ->where('firmo', 'SI')
        ->where('centro', $request->centro)->get();

        return [
            'info_centro' =>  $info_centro,
            'datos'		  =>  $datos,
            'categories'  =>  $categorias,
            'series'      =>  $series,
            'militantes'  =>  $militantes,
            'mesas'		  =>  $mesas,
            'firmantes'   =>  $firmantes,
            'carnet'       =>  $carnet->total

        ];
    }

	public function centrosmesa(Request $request){


		$grafica = ElecionesResultados::select([
			//'ElecionesResultados2.descrip_eleccion',
			'elecciones.nombre',
			'elecciones.fecha',
			DB::raw('Sum(eleciones_resultados.oficialismo) as votosoficiales'),
			DB::raw('Sum(eleciones_resultados.oposicion) as votosoposicion'),

		])
		->leftJoin('elecciones', 'elecciones.id','=', 'eleciones_resultados.elecciones_id')
		->leftJoin('centros', 'eleciones_resultados.centros_id','=', 'centros.id')
		//->groupby('ElecionesResultados.descrip_eleccion')
		->groupby('elecciones.fecha')
		->groupby('elecciones.nombre')
		->groupby('elecciones.tipo')
        //->orderBy('elecciones.nombre', 'asc')
        ->orderBy('elecciones.fecha', 'asc')
		->where('eleciones_resultados.mesa', intval($request->mesa))
		->where('centros.id', $request->centro);



		$categorias = [];
		$series['psuv'] = [
            'name' => 'Psuv'
        ];
		$series['oposicion'] = [
            'name' => 'Oposicion'
        ];

		foreach ($grafica->get() as $key => $value) {
			$categorias[]= $value->nombre;
			$series['psuv']['data'][] = intval($value->votosoficiales);
			$series['oposicion']['data'][] = intval($value->votosoposicion) ;

		}

		$series = array_values($series);


        return [

            'categories'  =>  $categorias,
            'series'      =>  $series,

        ];
    }

    protected function mesas_electorales($centro){

    	$votantes_psuv = RepBolivar::select([
			DB::raw('count(*) as total'),
			'psuv',
			'mesa'
		])
		->groupby('psuv')
		->groupby('mesa')
		->where('cod_centro', $centro )
		->where('psuv', 'si' );

		$votantes_por_mesas = RepBolivar::select([
			DB::raw('count(*) as total'),
			'mesa'
		])

		->groupby('mesa')
		->where('cod_centro', $centro );

        $votantes_por_firmaron = RepBolivar::select([
			DB::raw('count(firmantes_rev.cedula) as total'),
			'mesa'
		])
        ->leftJoin('firmantes_rev', 'firmantes_rev.cedula','=', 'rep_bolivar.cedula')
		->groupby('mesa')
		->where('cod_centro', $centro );


		$tabla = [];

        $_firmantes = DB::connection('bolivar_psuv')
        ->table('rep_psuv2010f')
        ->select([
            DB::raw('Count(centro) as total'),
            'mesa'
        ])
        ->groupby('mesa')
        ->where('firmo', 'SI')
        ->where('centro', $centro)->get();

        $carnet_mesa = RepBolivar::select([
			DB::raw('count(s_v_carnet_patria.cedula) as total'),
			'rep_bolivar.mesa'
		])
        ->leftJoin('s_v_carnet_patria', 's_v_carnet_patria.cedula','=', 'rep_bolivar.cedula')
		->groupby('rep_bolivar.mesa')
		->where('s_v_carnet_patria.codigo_centro', $centro)->get();



		foreach ($votantes_psuv->get() as $value) {
			$tabla[$value->mesa]['mesa']= $value->mesa;
			$tabla[$value->mesa]['psuv'][]= $value->total;
        }
        foreach ($_firmantes as $key => $value) {
            $tabla[$value->mesa]['firmantes'][] =  $value->total;
        }
		foreach ($votantes_por_mesas->get() as $value) {
			$tabla[$value->mesa]['mesa']= $value->mesa;
			$tabla[$value->mesa]['electores'][]= $value->total;
		}
		foreach ($carnet_mesa as $value) {
			$tabla[$value->mesa]['mesa']= $value->mesa;
			$tabla[$value->mesa]['carnet'][]= $value->total;
		}

		return $tabla;
    }

	protected function militantes($centro){

    	$militantes = RepBolivar::select([
			DB::raw('count(*) as total'),
			'psuv'
		])
		->groupby('psuv')
		->where('cod_centro', $centro )
		->where('psuv', 'si')->get();


		return  $militantes;
    }

    protected function tabla_centro($centro){
    	$_datos = ElecionesResultados::select([
			'elecciones.nombre',
			'centros.nombre as municipio',
            'eleciones_resultados.mesa',
			DB::raw('Sum(eleciones_resultados.oficialismo) as votosoficiales'),
			DB::raw('Sum(eleciones_resultados.oposicion) as votosoposicion'),

		])
		->leftJoin('elecciones', 'elecciones.id','=', 'eleciones_resultados.elecciones_id')
		->leftJoin('centros', 'eleciones_resultados.centros_id','=', 'centros.id')
		->whereIn('elecciones.nombre', [
			"Regionales 2008",
			"Elecciones Parlamentarias 2010",
			"Presidenciales 2012",
			"Regionales 2012",
			"Presidenciales 2013",
			"Elecciones Parlamentarias 2015",
			'Elecciones Regionales 2017'
		])
		->where('centros.nombre','!=', '')
        ->where('centros_id', $centro)
		->orderBy('centros.nombre', 'asc')
		->orderBy('eleciones_resultados.mesa', 'asc')
		->groupby('elecciones.nombre')
		->groupby('eleciones_resultados.mesa')
		->groupby('centros.nombre')
		->orderBy('elecciones.fecha', 'asc');
		//->where('centros.parroquia', $request->tipo);

        $datos =[];
		$resultado = 0;

		foreach ($_datos->get() as $value) {
			$resultado = 0;
			if($value->votosoficiales > $value->votosoposicion){
				$votosoficiales = strval($value->votosoficiales)."(G)";
				$resultado = $value->votosoficiales - $value->votosoposicion;
			}else{
				$votosoficiales = strval($value->votosoficiales)."(P)";
				$resultado = $value->votosoficiales - $value->votosoposicion;
			}

			$datos[$value->mesa]['mesa'] = $value->mesa;
			$datos[$value->mesa][$value->nombre] = [
				'votosoficiales' => $votosoficiales,
				'votosoposicion' => $value->votosoposicion,
				'resultado'		 => $resultado
			];
		}

		return $datos;
    }

    public function elecciones(){

    	return Elecciones::pluck('nombre', 'id');
    }

    public function comparar(Request $request){

        $_datos = ElecionesResultados::select([
			'elecciones.nombre',
			'centros.municipio',
			DB::raw('Sum(eleciones_resultados.oficialismo) as votosoficiales'),
			DB::raw('Sum(eleciones_resultados.oposicion) as votosoposicion'),
		])
		->leftJoin('elecciones', 'elecciones.id','=', 'eleciones_resultados.elecciones_id')
		->leftJoin('centros', 'eleciones_resultados.centros_id','=', 'centros.id')
		->where('centros.municipio','!=', '')
		->orderBy('centros.municipio', 'asc')
		->groupby('elecciones.nombre')
		->groupby('centros.municipio')
        ->whereIn('elecciones.id',[$request->elecciones_1, $request->elecciones_2])
		->orderBy('elecciones.fecha', 'asc');

		$datos =[];
		$resultado = 0;
        $elecciones = [];
		foreach ($_datos->get() as $value) {
			$resultado = 0;
			if($value->votosoficiales > $value->votosoposicion){
				$votosoficiales = strval($value->votosoficiales)."(G)";
				$resultado = $value->votosoficiales - $value->votosoposicion;
			}else{
				$votosoficiales = strval($value->votosoficiales)."(P)";
				$resultado = $value->votosoficiales - $value->votosoposicion;
			}
			$elecciones[$value->nombre] = $value->nombre;
			$datos[$value->municipio]['municipios'] = $value->municipio;
			$datos[$value->municipio][$value->nombre] = [
				'votosoficiales' => $votosoficiales,
				'votosoposicion' => $value->votosoposicion,
				'resultado'		 => $resultado
			];
		}


		return [
            'datos' => $datos,
            'elecciones' => $elecciones
        ];
    }

    public function excel(Request $request, $centro_id = 0,$quieres = 0, $mesas= 0 ){
        $data =[];
        $data['id'] = $centro_id;
        $data['quieres'] = $quieres;
        $data['mesa'] = $mesas;


        Excel::create('Excel', function($excel) use($data) {
            $reporte = "Electores del centro ".  $data['id'];

			if($data['quieres'] == 'militante'){
                $reporte = "militante del centro ".  $data['id'];
            }
            if($data['quieres'] == 'firmantes'){
                $reporte = "firmantes del centro ".  $data['id'];
            }
            if($data['quieres'] == 'carnet'){
                $reporte = "Personas con Carnet de la patria del centro ".$data['id'];
            }

            $excel->setTitle($reporte);
                // Chain the setters
            $excel->setCreator('Dirección de Informática y Sistemas')
            ->setCompany('GEB');

            // Call them separately


        	$excel->sheet('datos', function($sheet) use($data) {

                $repuesta = RepBolivar::select([
                    "rep_bolivar.cedula",
                    "rep_bolivar.nombre_rep",
                    "rep_bolivar.fecha_nacimiento",
                    "rep_bolivar.sexo"
                ]);

                if($data['quieres'] == 'electores'){
                    $repuesta->where('rep_bolivar.cod_centro',  $data['id']);
                }

                if($data['quieres'] == 'mesa'){
                    $repuesta->where('rep_bolivar.cod_centro',  $data['id'])
                    ->where('mesa', $data['mesa']);
                }

				if($data['quieres'] == 'mesa-militante'){
                    $repuesta->where('rep_bolivar.cod_centro',  $data['id'])
                    ->where('mesa', $data['mesa']);
					$repuesta->where('psuv', 'si');
				}
                if($data['quieres'] == 'militante'){
                    $repuesta->where('rep_bolivar.cod_centro',  $data['id'])
                    ->where('psuv', 'si');
                }
                if($data['quieres'] == 'mesa-carnet'){

                   $repuesta->where('s_v_carnet_patria.codigo_centro',  $data['id'])
                   ->where('rep_bolivar.mesa', $data['mesa'])
                   ->leftJoin('s_v_carnet_patria', 's_v_carnet_patria.cedula','=', 'rep_bolivar.cedula');
                }

                if($data['quieres'] == 'carnet'){

                   $repuesta->where('s_v_carnet_patria.codigo_centro',  $data['id'])
                    ->leftJoin('s_v_carnet_patria', 's_v_carnet_patria.cedula','=', 'rep_bolivar.cedula');
                }

				$res = $repuesta->get();


				if($data['quieres'] == 'mesa-firmantes'){
					$reporte = "firmantes del centro ".  $data['id'];
					$repuesta = DB::table('rep_psuv2010f')
					->select([
						'cedula',
						'primer_nombre',
						'primer_apellido',
						'fnac'
					])
					->where('firmo', 'SI')
					->where('centro', $data['id'])->where('mesa', $data['mesa']);

					$res =  $repuesta->get()->toArray();
				}


				if($data['quieres'] == 'firmantes'){

                    $reporte = "firmantes del centro ".  $data['id'];

                    $repuesta = DB::table('rep_psuv2010f')
                    ->select([
                        'cedula',
                        'primer_nombre',
                        'primer_apellido',
                        'fnac'
                    ])
                    ->where('firmo', 'SI')
                    ->where('centro', $data['id']);

                    $res =  $repuesta->get();
                }

                foreach ($res as $value) {


                    if($data['quieres'] == 'firmantes' || $data['quieres'] == 'mesa-firmantes' ){

                        $datos[]=[
                            'cedula'    => $value['cedula'],
                            'nombre'	=> $value['primer_nombre'],
                            'apellido'	=> $value['primer_apellido'],
                            'fnac'      => $value['fnac'],
                        ];
                    }else{

                        $datos[]=[
                            'cedula'            => $value['cedula'],
                            'nombre y apellidos'=> $value['nombre_rep'],
                            'fnac'              => $value["fecha_nacimiento"],
                            'sexo'              => $value["sexo"],
                        ];
                    }
                }

                $sheet->fromArray($datos);
            });

        })->download('xlsx');

    }


}
