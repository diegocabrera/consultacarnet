<?php

Route::group(['middleware' => 'web', 'prefix' => 'api', 'namespace' => 'App\\Modules\Api\Http\Controllers'], function()
{
    //Route::get('/', 'ApiController@index');
    Route::get('consultar/{id}', 	 'ApiController@consulta_api');
    Route::get('consultar/', 	 'ApiController@consulta_api');
});
