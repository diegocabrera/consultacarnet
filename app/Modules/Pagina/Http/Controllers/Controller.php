<?php

namespace App\Modules\Pagina\Http\Controllers;

use App\Http\Controllers\Controller as BaseController;

class Controller extends BaseController {
	public $app = 'Pagina';

    public $autenticar = false;


	protected $patch_js = [
		'public/js',
		'public/plugins',
		'app/Modules/Pagina/Assets/js',
	];

	protected $patch_css = [
		'public/css',
		'public/plugins',
		'app/Modules/Pagina/Assets/css',
	];

	public $libreriasIniciales = [

	];

}
