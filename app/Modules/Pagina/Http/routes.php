<?php

Route::group(['middleware' => 'web', 'prefix' => 'pagina', 'namespace' => 'App\\Modules\Pagina\Http\Controllers'], function()
{
    Route::get('consultar/', 'PaginaController@index');
});
