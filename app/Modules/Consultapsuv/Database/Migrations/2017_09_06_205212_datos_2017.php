<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Datos2017 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('centros', function (Blueprint $table) {
            $table->integer('id')->unsigned()->unique();

            $table->string('nombre', 200);
            $table->string('municipio', 100);
            $table->string('parroquia', 100);
            $table->string('direccion', 200);

            $table->integer('mesas')->unsigned();
            $table->integer('votantes')->unsigned(); 
           
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('elecciones', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('nombre', 50);
            $table->string('fecha', 10);
            $table->char('tipo', 1);

            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('eleciones_resultados', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('elecciones_id')->unsigned();
            $table->integer('centros_id')->unsigned();

            $table->tinyInteger('mesa')->unsigned(); 
            
            $table->integer('oficialsmo')->unsigned(); 
            $table->integer('oposicion')->unsigned(); 
            $table->integer('total_electores')->unsigned(); 
            $table->integer('participacion')->unsigned(); 
            $table->integer('abstencion')->unsigned(); 
            $table->integer('nulos')->unsigned(); 
           
            $table->foreign('elecciones_id')
                ->references('id')->on('elecciones')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('centros_id')
                ->references('id')->on('centros')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eleciones_resultados');
        Schema::dropIfExists('elecciones');
        Schema::dropIfExists('centros');
    }
}