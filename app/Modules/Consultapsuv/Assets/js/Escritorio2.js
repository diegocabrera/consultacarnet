var info_centro_id;
var info_centro_mesa;
var socio_politico = true;
var ayuda_social = true;
$('#busqueda_carnet').on('click', function() {
    if ($('#carnet').val() != '') {
        datos_personas($('#ci').val());
    }
});

$("#carnet").keyup(function(event) {
    if (event.which == 13) {
        if ($('#carnet').val() != '') {
            datos_personas($('#ci').val());
        }
    }
});
$('#busqueda_ci').on('click', function() {
    var id = $('#ci').val();
    if ($('#ci').val() != '') {
        datos_personas(id);
    }

});

$("#ci").keyup(function(event) {

    if (event.which == 13) {
        if ($('#ci').val() != '') {
            datos_personas($('#ci').val());
        }
    }
});


$('#socio_politico').on('click', function() {
    if (socio_politico) {
        $('#accordion4').css('display', 'block');
        socio_politico = false;
    } else {
        $('#accordion4').css('display', 'none');
        socio_politico = true;
    }
});
$('#ayuda_social').on('click', function() {
    if (ayuda_social) {
        $('#accordion5').css('display', 'block');
        ayuda_social = false;
    } else {
        $('#accordion5').css('display', 'none');
        ayuda_social = true;
    }
});



function datos_personas($ci) {
    // var win = window.open(dire + '/inicio/consulta/' + $ci, 'nuevo', 'height=500,width=1000,resizable=yes,scrollbars=yes');
    // win.focus();

    const url = "http://consultacarnet.e-bolivar.gob.ve/";
    $.ajax({
        'url': 'consultar/' + $ci,
        success: function(r) {
            info_centro_id = r.info_centro['id'];
            info_centro_mesa = r.informacion_personal['mesa'];
            console.log(info_centro_mesa);
            pat_territoriales_tmunicipios = r.pat_territoriales_tmunicipios;
            console.log();
            const foto = dire + "/public/img/fotos/" + r.informacion_personal['dni'] + ".jpg";
            const check = dire + "/public/img/check.png";
            const no_check = dire + "/public/img/check2.png";
            const id = r.informacion_personal['dni'];
            info_centro(info_centro_id, info_centro_mesa);
            $('#modal_centro').on('click', function() {
                modal_centro();
                $('#exampleModalLong').modal('show');
            });
            $('#modal_territoriales_m').on('click', function() {
                modal_territorial();
                $('#exampleModalLong-2').modal('show');
            });
            $('#modal_territoriales_s').on('click', function() {
                modal_sectorial()
                $('#exampleModalLong-3').modal('show');
            });
            if (foto) {
                $('#foto_').attr('src', foto);
            } else {
                console.log(foto);
                console.log(url);
                console.log('no tiene foto');
            }

            $('#nombr_').text(r.informacion_personal['nombre']);
            $('#dni_').text(r.informacion_personal['dni']);
            $('#fech_nac_').text(r.informacion_personal['nacimiento']);
            $('#edad_').text(r.informacion_personal['edad']);
            $('#municipio_').text(r.rep_psuv2010['municipio_nombre']);
            $('#parroquia_').text(r.rep_psuv2010['parroquia_nombre']);
            $('#direccion_').text(r.rep_psuv2010['direccion']);
            $('#telf_').text(r.informacion_personal['telefono']);
            $('#correo_').text(r.informacion_personal['telefono']);
            $('#tw_').text();
            $('#ig_').text();
            var cp;
            if (r.informacion_personal['cp'] == 1) {
                cp = 'SI';
                $('#cp_').text(cp);
            } else {
                cp = 'NO';
                $('#cp_').text(cp);
            }
            $('#estatus_').text(r.patria_carnet['estatus']);
            $('#subambito_').text(r.patria_carnet['sub_ambito']);
            $('#parentesco_').text(r.patria_carnet['parentesco']);
            $('#ambespec_').text(r.patria_carnet['ambito_especifico']);
            if (r.varios_votos['v7oct'] == "SI") {
                $('#v7oct_').attr('src', check);
            } else {
                $('#v7oct_').attr('src', no_check);
            }
            if (r.varios_votos['v14abr'] == "SI") {
                $('#v14abr_').attr('src', check);
            } else {
                $('#v14abr_').attr('src', no_check);
            }
            if (r.varios_votos['v28jul'] == "SI") {
                $('#v28jul_').attr('src', check);
            } else {
                $('#v28jul_').attr('src', no_check);
            }
            if (r.varios_votos['primud'] == "SI") {
                $('#primud_').attr('src', check);
            } else {
                $('#primud_').attr('src', no_check);
            }
            if (r.varios_votos['v6d'] == "SI") {
                $('#v6d_').attr('src', check);
            } else {
                $('#v6d_').attr('src', no_check);
            }
            if (r.varios_votos['ubch'] == "SI") {
                $('#ubch_').attr('src', check);
            } else {
                $('#ubch_').attr('src', no_check);
            }
            if (r.varios_votos['firmo'] == "SI") {
                $('#firma_chavez_').attr('src', check);
            } else {
                $('#firma_chavez_').attr('src', no_check);
            }
            if (r.varios_votos['firma_maduro'] == "SI") {
                $('#firma_maduro_').attr('src', check);
            } else {
                $('#firma_maduro_').attr('src', no_check);
            }
            $('#cod_cnt_elect_').text(r.info_centro['id']);
            $('#nbr_cnt_elect_').text(r.info_centro['nombre']);
            $('#n_mesa_').text(r.informacion_personal['mesa']);
            $('#t_mesa_').text(r.info_centro['tecnologia']);
            $('#elect_').text(r.info_centro['electores']);
            $('#mil_psuv_rep_').text(r.info_centro['militantes']);
            $('#firmaron_').text(r.info_centro['votantes']);
            $('#reg_2011_').text('xxxxx');
            $('#direc_centro_').text(r.info_centro['votantes']);
            $('#parroquia_centro_').text(r.info_centro['parroquia']);
            $('#municipio_centro_').text(r.info_centro['municipio']);
            $('#direc_centro_').text(r.info_centro['direccion']);
            if (r.informacion_personal['psuv'] == 1) {
                $('#inscrito_psuv_').attr('src', check);
            } else {
                $('#inscrito_psuv_').attr('src', no_check);
            }
            if (r.inscritos['inscrito_2007'] != 'NO') {
              $('#inscrito_2007_').attr('src', check);
            } else {
              $('#inscrito_2007_').attr('src', no_check);
            }
            if (r.inscritos['inscrito_2009'] == 'SI') {
                $('#inscrito_2009_').attr('src', check);
            } else {
                $('#inscrito_2009_').attr('src', no_check);
            }
            if (r.inscritos['actualizado_2009'] == 'SI') {
                $('#actualizado_2009_').attr('src', check);
            } else {
                $('#actualizado_2009_').attr('src', no_check);
            }
            if (r.inscritos['inscrito_2010'] == 'SI') {
                $('#inscrito_2010_').attr('src', check);
            } else {
                $('#inscrito_2010_').attr('src', no_check);
            }
            if (r.hogares_patria['direccion_hp'] !== '') {
                $('#hogares_patria_').attr('src', check);
            } else {
                $('#hogares_patria_').attr('src', no_check);
            }
            if (r.ccc2012['cargo_cce'] !== '') {
                $('#cargo_cce_').attr('src', check);
            } else {
                $('#cargo_cce_').attr('src', no_check);
            }
            if (r.ccc_municipales_2013['cargo_ccm'] != '') {
                $('#cargo_ccm_').attr('src', check);
            } else {
                $('#cargo_ccm_').attr('src', no_check);
            }
            if (r.ccc_parroquiales_2013['cargo_ccp'] !== '') {
                $('#cargo_ccp').attr('src', check);
            } else {
                $('#cargo_ccp').attr('src', no_check);
            }
            if (r.anillo_2010['anillo'] != '') {
                $('#anillo_').attr('src', check);
                $('#anillo_t').text(r.anillo_2010['anillo']);
            } else {
                $('#anillo_').attr('src', no_check);
                $('#anillo_t').text('');
            }
            if (r.ubc_resp1x10['codigo_centro_ubch_1x10'] != '') {
                $('#1x10_resp_centro_').attr('src', check);
                $('#1x10_resp_centro_t').text(r.ubc_resp1x10['codigo_centro_ubch_1x10']);
            } else {
                $('#1x10_resp_centro_').attr('src', no_check);
                $('#1x10_resp_centro_t').text('');
            }
            /* if (r.ubc_resp1x10['codigo_centro_ubch_1x10'] != '') {
                $('#1x10_resp_centro_').attr('src', check);
                $('#1x10_resp_centro_t').text(modal_centro());
            } else {
                $('#1x10_resp_centro_').attr('src', no_check);
                $('#1x10_resp_centro_t').text('');
            } */
            if (r.pat_territoriales_tmunicipios['ced_resp'] != '') {
                $('#pat_terr_').attr('src', check);
            } else {
                $('#pat_terr_').attr('src', no_check);
            }
            $('#cod_patrulla_terr_').text(r.pat_territoriales_tmunicipios['codigo_pat_territorial']);
            if (r.pat_territoriales_tmunicipios['ced_resp'] !== r.pat_territoriales_tmunicipios['ced_patrulla']) {
                $('#responsable_patrulla').text('NO');
            } else {
                $('#responsable_patrulla').text('SI');
            }
            if (r.ayudas != '') {
                $('#ultima_ayuda').text(r.ayudas['ultima_ayuda']);
                $('#observacion_ayuda').text(r.ayudas['ultima_observacion']);
                $('#tipo_ayuda').text(r.ayudas['ultima_ayuda_tipo']);
                $('#ayudas_solicitadas').text(r.ayudas['ayudas_solicitadas']);
            } else {
                $('#ultima_ayuda').text('No ha Solicitado ayudas');
                $('#observacion_ayuda').text('');
                $('#tipo_ayuda').text();
                $('#ayudas_solicitadas').text('Nunca ha realizado una solicitud de ayudas');
            }









            //graficas();

            function modal_centro() {
                if (visto >= 1) {
                    $tabla.ajax.reload();
                    return;
                }

                visto = 1;

                $tabla = $('#tabla').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: dire + '/consulta/datacentro',
                        data: function(d) {
                            d.ci = "{{$ci}}"
                        }
                    },
                    language: datatableEspanol,
                    columns: [
                        { data: 'cedula_mi1x10', name: 'miembro1x10.cedula_mi1x10' },
                        { data: 'nombres_mi1x10', name: 'miembro1x10.nombres_mi1x10' },
                        { data: 'apellidos_mi1x10', name: 'miembro1x10.apellidos_mi1x10' },
                        { data: 'centro_mi1x10', name: 'miembro1x10.centro_mi1x10' },
                        { data: 'telefono_mi1x10', name: 'miembro1x10.telefono_mi1x10' },
                        { data: 'centro', name: 'centro' }
                    ]
                });

                $('#tabla').on("click", "tbody tr", function() {
                    var win = window.open(dire + '/inicio/consulta/' + this.id, 'nuevo2', 'height=500,width=1000,resizable=yes,scrollbars=yes');
                    win.focus();
                });
            }

            function modal_territorial() {
                if (visto2 >= 1) {
                    $tabla2.ajax.reload();
                    return;
                }

                visto2 = 1;

                $tabla2 = $('#tabla2').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: dire + '/consulta/dataterimunicipio',
                        data: function(d) {
                            d.ci = pat_territoriales_tmunicipios['codigo_pat_territorial'];
                        }
                    },
                    language: datatableEspanol,
                    columns: [
                        { data: 'ced_patrulla', name: 'pat_territoriales_tmunicipios.ced_patrulla' },
                        { data: 'nombre_rep', name: 'rep_bolivar.nombre_rep' },
                        { data: 'des_municipio', name: 'pat_territoriales_tmunicipios.des_municipio' },
                        { data: 'desp_parroquia', name: 'pat_territoriales_tmunicipios.desp_parroquia' },

                    ]
                });

                $('#tabla2').on("click", "tbody tr", function() {
                    var win = window.open(dire + '/inicio/consulta/' + this.id, 'nuevo53', 'height=500,width=1000,resizable=yes,scrollbars=yes');
                    win.focus();
                });

            }

            function modal_sectorial() {
                if (visto3 >= 1) {
                    $tabla3.ajax.reload();
                    return;
                }

                visto3 = 1;

                $tabla3 = $('#tabla3').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: dire + '/consulta/dataterisect',
                        data: function(d) {
                            d.ci = "{{$pat_territoriales_tmunicipios->codigo_pat_territorial}}";
                        }
                    },
                    language: datatableEspanol,
                    columns: [
                        { data: 'cedula_patrulla_sect', name: 'cedula_patrulla_sect' },
                        { data: 'NOMBRES', name: 'NOMBRES' },
                        { data: 'APELLIDOS', name: 'APELLIDOS' },
                        { data: 'TELEFONO_RESIDENCIA', name: 'TELEFONO_RESIDENCIA' },
                        { data: 'MUNICIPIO_NOMBRE', name: 'MUNICIPIO_NOMBRE' },
                        { data: 'PARROQUIA_NOMBRE', name: 'PARROQUIA_NOMBRE' },
                        { data: 'GEOGRAFICO_DES', name: 'GEOGRAFICO_DES' },
                        { data: 'SECTOR', name: 'SECTOR' },
                        { data: 'URBANIZACION', name: 'URBANIZACION' },

                    ]
                });

                $('#tabla3').on("click", "tbody tr", function() {
                    var win = window.open(dire + '/inicio/consulta/' + this.id, 'nuevo553', 'height=500,width=1000,resizable=yes,scrollbars=yes');
                    win.focus();
                });

            }

            function graficas_centro(series, categories) {
                var chart = Highcharts.chart('grafica_centro', {
                    title: {
                        text: ''
                    },

                    subtitle: {
                        text: ''
                    },

                    xAxis: {
                        categories: categories,
                        tickmarkPlacement: 'on',
                        title: {
                            enabled: 'false'
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: '',
                            align: 'high'
                        },
                        labels: {
                            overflow: 'justify'
                        }
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}'
                    },
                    colors: ["#b93f3f", "#7cb5ec"],
                    plotOptions: {
                        bar: {

                            colorByPoint: true,
                            dataLabels: {
                                text: ' ',
                                enabled: true
                            },
                            groupPadding: 0.05
                        },
                        series: {
                            borderWidth: 0,
                            dataLabels: {
                                enabled: true,
                                format: '{point.y}'
                            }
                        }
                    },
                    series: series
                });

            }

            function info_centro(centro, mesa) {
                console.log(centro);
                console.log(mesa);
                $.ajax({
                    url: dire + '/inicio/centromesa',
                    type: 'POST',
                    data: {
                        'centro': centro,
                        'mesa': mesa,
                    },
                    success: function(r) {
                        console.log(r);
                        graficas_centro(r.series, r.categories);

                    }
                });

            }

        }
    });
}

var $tabla, $tabla2, $tabla3, visto = 0,
    visto2 = 0,
    visto3 = 0;

var datatableEspanol = {
    "sProcessing": "Procesando...",
    "sLengthMenu": "Mostrar _MENU_ registros",
    "sZeroRecords": "No se encontraron resultados",
    "sEmptyTable": "Ningún dato disponible en esta tabla",
    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix": "",
    "sSearch": "Buscar:",
    "sUrl": "",
    "sInfoThousands": ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst": "Primero",
        "sLast": "Último",
        "sNext": "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    }
};
