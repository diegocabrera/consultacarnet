<?php

namespace App\Modules\Consultapsuv\Http\Requests;

use App\Http\Requests\Request;

class ResultadoCentroRequest extends Request {
    protected $reglasArr = [
		'idcentro' => ['required', 'integer'], 
		'nromesa' => ['required', 'integer'], 
		'ideleccion' => ['required', 'integer'], 
		'totalelectores' => ['integer'], 
		'votosoficiales' => ['integer'], 
		'votosoposicion' => ['integer'], 
		'votosnulos' => ['integer']
	];
}