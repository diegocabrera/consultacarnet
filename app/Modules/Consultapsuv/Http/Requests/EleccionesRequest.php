<?php

namespace App\Modules\Consultapsuv\Http\Requests;

use App\Http\Requests\Request;

class EleccionesRequest extends Request {
    protected $reglasArr = [
		'nombre' => ['required', 'min:3', 'max:50'], 
		'fecha' => ['required', 'min:3', 'max:10'], 
		'tipo' => ['required', 'min:3', 'max:1']
	];
}