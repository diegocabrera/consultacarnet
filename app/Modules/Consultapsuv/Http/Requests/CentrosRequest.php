<?php

namespace App\Modules\Consultapsuv\Http\Requests;

use App\Http\Requests\Request;

class CentrosRequest extends Request {
    protected $reglasArr = [
		'id' => ['required', 'integer', 'unique:centros,id'], 
		'nombre' => ['required', 'min:3', 'max:200'], 
		'municipio' => ['required', 'min:3', 'max:100'], 
		'parroquia' => ['required', 'min:3', 'max:100'], 
		'direccion' => ['required', 'min:3', 'max:200'], 
		'mesas' => ['required', 'integer'], 
		'votantes' => ['required', 'integer']
	];
}