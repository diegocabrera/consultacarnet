<?php

namespace App\Modules\Consultapsuv\Http\Requests;

use App\Http\Requests\Request;

class DatosCentrosRequest extends Request {
    protected $reglasArr = [
		'codcentro' => ['min:3', 'max:100'], 
		'nomcentro' => ['min:3', 'max:200'], 
		'municipio_centro' => ['min:3', 'max:100'], 
		'parroquia_centro' => ['min:3', 'max:100'], 
		'direccion_centro' => ['min:3', 'max:100']
	];
}