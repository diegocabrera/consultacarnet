<?php

Route::group(['middleware' => 'web', 'prefix' => Config::get('admin.prefix'), 'namespace' => 'App\\Modules\Consultapsuv\Http\Controllers'], function()
{
    Route::get('/', 'EscritorioController@getIndex');
    Route::get('/patria_carnet', 'EscritorioController@patria_carnet');

    Route::get('/', function () {
    	return redirect('escritorio/escritorio');
    });

    // Route::group(['prefix' => 'api'], function (){
    //     Route::get('consultar/{id}', 	 'ApiController@consulta_api');
    //     //Route::get('api_ayudas',         'ApiController@ayudas');
    //     //Route::get('/', 				'ApiController@getIndex');
	// 	Route::post('/grafica',         'ApiController@grafica');
	//     Route::post('/graficatorta',    'ApiController@graficatorta');
	//     //Route::get('/consulta/{id}', 	'ApiController@consulta');
	//     //Route::get('/consult/{id}', 	'ApiController@consulta');
	//   /*Route::get('/consulta/{id}', 	'ApiController@personascentro');*/
	//     Route::post('/municipios', 		'ApiController@tablacomparacion');
	//     Route::post('/parroquias', 		'ApiController@comparacionparroquias');
	//     Route::post('/centros', 		'ApiController@comparacioncentros');
	//     Route::post('/infocentro', 		'ApiController@centros');
	//     Route::post('/centromesa', 		'ApiController@centrosmesa');
	//     Route::post('/comparar', 		'ApiController@comparar');
	//     //Route::get('/excel/{centro_id}/{quieres}/{mesas}', 		    'ApiController@excel');
    // });

  	Route::group(['prefix' => 'inicio'], function() {
		Route::get('/', 				'EscritorioController@getIndex');
		Route::post('/grafica',         'EscritorioController@grafica');
	    Route::post('/graficatorta',    'EscritorioController@graficatorta');
	    Route::get('/consulta/{id}', 	'EscritorioController@consulta');
	    Route::get('/consult/{id}', 	'EscritorioController@consulta');
	  /*Route::get('/consulta/{id}', 	'EscritorioController@personascentro');*/
	    Route::post('/municipios', 		'EscritorioController@tablacomparacion');
	    Route::post('/parroquias', 		'EscritorioController@comparacionparroquias');
	    Route::post('/centros', 		'EscritorioController@comparacioncentros');
	    Route::post('/infocentro', 		'EscritorioController@centros');
	    Route::post('/centromesa', 		'EscritorioController@centrosmesa');
	    Route::post('/comparar', 		'EscritorioController@comparar');
	    Route::get('/excel/{centro_id}/{quieres}/{mesas}', 		    'EscritorioController@excel');
    });
  	Route::group(['prefix' => 'escritorio'], function() {
		Route::get('/escritorio', 		'EscritorioController@index2');
		Route::get('consulta/{id}', 	 'EscritorioController@consulta');
		Route::get('consultar/{id}', 	 'EscritorioController@consulta_api');

    });
  	Route::group(['prefix' => 'consulta'], function() {
		Route::get('/datacentro', 			'EscritorioController@personascentro');
		Route::get('/dataterimunicipio', 	'EscritorioController@dataterimunicipio');
		Route::get('/dataterisect', 	    'EscritorioController@dataterisect');
    });

   //{{route}}
   Route::group(['prefix' => 'rep'], function() {
		Route::get('/', 				'RepController@index');
		Route::get('buscar/{id}', 		'RepController@buscar');
		Route::get('nuevo', 		    'RepController@nuevo');

		Route::get('cambiar/{id}', 		'RepController@cambiar');
		Route::post('guardar',			'RepController@guardar');
		Route::put('guardar/{id}', 		'RepController@guardar');

		Route::delete('eliminar/{id}', 	'RepController@eliminar');
		Route::post('restaurar/{id}', 	'RepController@restaurar');
		Route::delete('destruir/{id}', 	'RepController@destruir');

		Route::post('cambio', 			'RepController@cambio');
		Route::get('datatable', 		'RepController@datatable');
	});
});
