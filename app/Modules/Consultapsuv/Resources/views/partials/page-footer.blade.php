	
	<!-- END PRE-FOOTER -->
	<!-- BEGIN INNER FOOTER -->
	<!--
		Diseñado: Maria Campora camporamaria@gmail.com  
		Desarrollado por: Ing.Miguelangel Gutierrez Drummermiguelangel@gmail.com
	 -->

	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="footer-top" style="height: 46px;">	
					<div class="col-md-12 color">
						<br>
			{{--  			<center><span style="color:white">Diseñado y desarrollado por la  Ing. Miguelangel Gutierrez Drummermiguelangel@gmail.com</center>  --}}
					</div>
					
				</div>

				<div class="col-md-12">
					<div class="footer-bottom">  
						<div class="row">
							<div class="col-md-12">
								<center> Todos los derechos reservados. Copyright &copy; {{ date('Y') }}  {{ $controller->conf('nombre_empresa') }}.</center>
							</div>
						</div>
					</div>
				</div>  
			</div>
		</div>
	</div>
