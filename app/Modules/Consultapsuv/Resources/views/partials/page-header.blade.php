    <div id="header" class="header-area header-one">
        {{--  <div class="nav-top">
            <div class="container">
                <div class="row">     
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="login-area right">
                            <b>Siguenos Por: </b> 
                            <a class="btn btn-social-icon btn-twitter"  href="https://twitter.com/psuv_bolivar"><span class="fa fa-twitter"></span></a>
                            <a class="btn btn-social-icon btn-facebook" href="https://www.facebook.com/psuvbolivar"><span class="fa fa-facebook"></span></a>
                            <a class="btn btn-social-icon btn-openid"   href="{{ url(Config::get('admin.prefix').'/') }}"><span class="fa fa-rss"></span></a>
                            <a class="btn btn-social-icon btn-google"   href=""><span class="fa fa-youtube"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>  --}}

        <div class="logo-area">
            <div class="container">
                <div class="row">
                    <div class="main-logo col-xs-12  col-lg-12 col-md-12 col-sm-12  pd-top-20 pd-bottom-20">
                        <div class="logo">
                            <a href="{{url('/')}}"><img class="retina img-responsive " src="{{ asset('public/img/logos/'.$controller->conf('logo')) }}" alt="logo" style="width: 145px;"></a>
                        </div>
                    </div>
                    {{--  <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6" id="buscardor">
                        <div class="input-group stylish-input-group">
                            <input type="text" class="form-control buscador"  placeholder="Introducir el número de cédula a buscar" id="ci" >
                            <span class="input-group-addon">
                                <button type="submit">
                                    <span class="glyphicon glyphicon-search" id="boton_busqueda"></span>
                                </button>  
                            </span>
                        </div>
                    </div>  --}}
                </div>
            </div>
        </div>
    </div>
    
     @include('consultapsuv::partials.menu')
   