<?php

namespace App\Modules\Consultapsuv\Models;

use App\Modules\Base\Models\Modelo;


class Centros extends Modelo
{
    protected $table = 'centros';
    protected $fillable = ["id","nombre","municipio","parroquia","direccion","mesas","votantes"];


}