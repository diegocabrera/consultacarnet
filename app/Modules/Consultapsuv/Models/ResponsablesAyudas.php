<?php
namespace App\Modules\Consultapsuv\Models;

use App\Modules\Base\Models\Modelo;
use Illuminate\Database\Eloquent\Model;


class ResponsablesAyudas extends Model
{
    protected $table = 'responsables_ayudas';
    protected $fillable = [
        "id",
        "nombre",
        "abreviatura",
        "nombre_director",
        "telefono",
        "fax",
        "email",
        "direccion",
        "funciones",
        "soluciones",
    ];



}
