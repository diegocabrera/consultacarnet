<?php

namespace App\Modules\Consultapsuv\Models;

use App\Modules\Base\Models\Modelo;
use Illuminate\Database\Eloquent\Model;


class Ayudas extends Model
{
    protected $table = 'ayudas';
    protected $fillable = [
        "id",
        "personas_ayudas_id",
        "resumen",
        "tipo_ayuda_id",
        "responsable_ayudas_id",
        "fecha_creacion",
    ];



}
