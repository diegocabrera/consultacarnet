<?php

namespace App\Modules\Consultapsuv\Models;

use App\Modules\Base\Models\Modelo;
use Illuminate\Database\Eloquent\Model;


class PersonasAyudas extends Model
{
    protected $table = 'personas_ayudas';
    protected $fillable = [
        "id",
        "cedula",
        "nombres"
    ];



}
