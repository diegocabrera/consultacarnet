<?php

namespace App\Modules\Consultapsuv\Models;

use App\Modules\Base\Models\Modelo;
use Illuminate\Database\Eloquent\Model;


class FirmantesRev extends Model
{
    protected $table = 'firmantes_rev';
    protected $fillable = ["id_firma_maduro","cedula_firmantes","firmo_revocatorio","voto_6d","trabaja"];
    protected $campos = [
    'id_firma_maduro' => [
        'type' => 'text',
        'label' => 'Id Firma Maduro',
        'placeholder' => 'Id Firma Maduro del Firmantes Rev'
    ],
    'cedula_firmantes' => [
        'type' => 'text',
        'label' => 'Cedula Firmantes',
        'placeholder' => 'Cedula Firmantes del Firmantes Rev'
    ],
    'firmo_revocatorio' => [
        'type' => 'text',
        'label' => 'Firmo Revocatorio',
        'placeholder' => 'Firmo Revocatorio del Firmantes Rev'
    ],
    'voto_6d' => [
        'type' => 'text',
        'label' => 'Voto 6D',
        'placeholder' => 'Voto 6D del Firmantes Rev'
    ],
    'trabaja' => [
        'type' => 'text',
        'label' => 'Trabaja',
        'placeholder' => 'Trabaja del Firmantes Rev'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        
    }

    
}