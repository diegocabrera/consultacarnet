<?php

namespace App\Modules\Consultapsuv\Models;

use App\Modules\Base\Models\Modelo;
use Illuminate\Database\Eloquent\Model;


class RequerimientosAyudas extends Model
{
    protected $table = 'requerimientos_ayudas';
    protected $fillable = [
        "id",
        "nombre",
        "responsables"
    ];



}
