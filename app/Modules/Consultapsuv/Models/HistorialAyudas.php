<?php

namespace App\Modules\Consultapsuv\Models;

use App\Modules\Base\Models\Modelo;
use Illuminate\Database\Eloquent\Model;


class HistorialAyudas extends Model
{
    protected $table = 'historial_ayudas';
    protected $fillable = [
        "id",
        "ayudas_id",
        "observacion"
    ];



}
