<?php
    $ci = 19729038;
?>

<?php $__env->startSection('content'); ?>
    <br><br>
    <div class="container">
        <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6" id="buscardor1">
            <div class="input-group stylish-input-group">
                <input type="text" class="form-control buscador"  placeholder="Introducir el número de cédula a buscar" id="ci" >
                <span class="input-group-addon">
                    <button type="submit">
                        <span class="glyphicon glyphicon-search" id="busqueda_ci"></span>
                    </button>
                </span>
            </div>
        </div>
        <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6" id="buscardor2">

            <div class="input-group stylish-input-group">
                <input type="text" class="form-control buscador"  placeholder="Introducir el serial del carnet de la patria" id="carnet" >
                <span class="input-group-addon">
                    <button type="submit">
                        <span class="glyphicon glyphicon-search" id="busqueda_carnet"></span>
                    </button>
                </span>
            </div>
        </div>
    </div>
    <br><br><br><br>
    <div id="consul_datos">
    <div class="container">
    <div class="row">

        <div class="col-xs-12  col-md-4">
            <div class="col-xs-12  col-md-12">
                <div class="widget-author  boxed  push-down-30">
                    <div class="widget-author__image-container">
                        <div class="widget-author__avatar--blurred">


                                <img src="<?php echo e(url('public/img/usuarios/user.png')); ?>" alt="Avatar image" width="90" height="90">


                        </div>

                            <img class="widget-author__avatar" src="<?php echo e(url('public/img/usuarios/user.png')); ?>" alt="Avatar image" width="90" height="90">

                    </div>
                    <div class="row">
                        <div class="col-xs-10  col-xs-offset-1">
                            <br><br><br>
                        </div>
                        <div class="xxx">
                            <div class="col-md-12 col-xs-12">
                                <div class="contenedor well-menu" data-tipo="1">
                                    <div class="well2" data-tipo="1">
                                        <center><h3>Informacion Personal</h3></center>
                                        <table class="table table-striped table-bordered table-hover table-condensed" style=" text-align: left; ">
                                            <tbody>
                                                <tr>
                                                    <td class="b"> <center><b>Nombres y Apellidos</b></center></td>
                                                    <td id ="nombr_"><!--  --></td>
                                                </tr>

                                                <tr>
                                                    <td><center><b>Cédula </b></center></td>
                                                    <td id ="dni_"><!--   --></td>
                                                </tr>

                                                <tr>
                                                    <td> <center><b>Fecha de Nacimiento </b></center></td>
                                                    <td id="fech_nac_"><!--  --></td>
                                                </tr>

                                                <tr>
                                                    <td> <center><b>Edad </b></center></td>
                                                    <td id="edad_"> <!--   --></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="contenedor well-menu" data-tipo="2">
                                    <div class="well2" data-tipo="2">
                                        <table class="table table-striped table-bordered table-hover table-condensed" style=" text-align: left;">
                                            <tbody>
                                                <tr>
                                                    <td><center><b>Municipio:</b></center></td>
                                                    <td id="municipio_"></td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Parroquia:</b></center></td>
                                                    <td id="parroquia_"></td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Direccion:</b></center></td>
                                                    <td id="direccion_"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="contenedor well-menu" data-tipo="3">
                                    <div class="well2" data-tipo="3">
                                        <center><h3>Contacto</h3></center>
                                        <table class="table table-striped table-bordered table-hover table-condensed" style=" text-align: left; ">
                                            <tbody>
                                                <tr>
                                                    <td><center><b>Teléfono:</b></td>
                                                    <td id="telf_"></td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Correo:</b></td>
                                                    <td id="correo_"></td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Twitter:</b></td>
                                                    <td id="tw_"></td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Instagram:</b></td>
                                                    <td id="ig_"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="contenedor well-menu" data-tipo="5">
                                    <div class="well2" data-tipo="4">
                                        <center><h3>Carnet de la patria</h3></center>
                                        <table class="table table-striped table-bordered table-hover table-condensed" style=" text-align: left; ">
                                            <tr>
                                                <td><center><b>Carnet de la patria</b></center></td>
                                                <td id="cp_">
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><center><b>Estatus:</b></center></td>
                                                <td id="estatus_"></td>
                                            </tr>
                                            <tr>
                                                <td><center><b>Sub Ambito:</b></center></td>
                                                <td id="subambito_"></td>
                                            </tr>
                                            <tr>
                                                <td><center><b>Parentesco:</b> </center></td>
                                                <td id="parentesco_"></td>
                                            </tr>
                                            <tr>
                                                <td><center><b>Ambito Especifico:</b></center></td>
                                                <td id="ambespec_"></td>
                                            </tr>
                                            <tr>
                                                <td> <center><b>Sub Ambito:</b></center> </td>
                                                <td id="subambito_"></td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
    </div>




<?php $__env->stopSection(); ?>
<?php $__env->startPush('js'); ?>
<script>

     var socio_politico = true;
     var ayuda_social = true;
     $('#busqueda_carnet').on('click', function() {
        if ($('#carnet').val() != '') {
           datos_personas($('#ci').val());
        }
    });

    $("#carnet").keyup(function(event) {
        if (event.which == 13) {
            if ($('#carnet').val() != '') {
                datos_personas($('#ci').val());
            }
        }
    });
     $('#busqueda_ci').on('click', function() {
        if ($('#ci').val() != '') {
           datos_personas($('#ci').val());
        }

        var id = $('#ci').val();

        

    });

    $("#ci").keyup(function(event) {
        if (event.which == 13) {
            if ($('#ci').val() != '') {
               datos_personas($('#ci').val());
            }
        }
    });

    $('#socio_politico').on('click', function(){
        if(socio_politico){
            $('#accordion4').css('display', 'block');
            socio_politico = false;
        }else{
            $('#accordion4').css('display', 'none');
             socio_politico = true;
        }
    });
    $('#ayuda_social').on('click', function(){
        if(ayuda_social){
            $('#accordion5').css('display', 'block');
            ayuda_social = false;
        }else{
            $('#accordion5').css('display', 'none');
             ayuda_social = true;
        }
    });



    function datos_personas($ci) {
        // var win = window.open(dire + '/inicio/consulta/' + $ci, 'nuevo', 'height=500,width=1000,resizable=yes,scrollbars=yes');
        // win.focus();
        $.ajax({
            'url'  : 'consultar/' + $ci ,
            success  : function(r){
                var foto = "public/img/fotos/"+r.informacion_personal['dni']+".jpg";
                console.log(r.informacion_personal['nombre']);
                $('#nombr_').text(r.informacion_personal['nombre']);
                $('#dni_').text(r.informacion_personal['dni']);
                $('#fech_nac_').text(r.informacion_personal['nacimiento']);
                $('#edad_').text(r.informacion_personal['edad']);
                $('#municipio_').text();
                $('#parroquia_').text();
                $('#direccion_').text();
                $('#telf_').text(r.informacion_personal['telefono']);
                $('#correo_').text(r.informacion_personal['telefono']);
                $('#tw_').text();
                $('#ig_').text();
                $('#estatus_').text();
                $('#subambito_').text();
                $('#parentesco').text();
                $('#ambespec_').text();

                if (foto === true) {
                    console.log(foto);
                    $('#foto_').attr('public/img/fotos/' + r.informacion_personal['dni'] + '.jpg');
                } else {
                    console.log('no tiene foto');
                }
                var cp;
                if (r.informacion_personal['cp'] == 1) {
                    cp='SI';
                    $('#cp_').text(cp);
                } else {
                    cp = 'NO';
                    $('#cp_').text(cp);
                }
            }
        });
    }

</script>
<?php $__env->stopPush(); ?>
<?php $__env->startPush('css'); ?>
<style>

.card {
    padding-top: 20px;
    margin: 10px 0 20px 0;
    background-color: rgba(214, 224, 226, 0.2);
    border-top-width: 0;
    border-bottom-width: 2px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.btn {
    width: 100%;!important;
    height: 32px;
    line-height: 18px;
}
.card .card-heading {
    padding: 0 20px;
    margin: 0;
}

.card .card-heading.simple {
    font-size: 20px;
    font-weight: 300;
    color: #777;
    border-bottom: 1px solid #e5e5e5;
}

.card .card-heading.image img {
    display: inline-block;
    width: 46px;
    height: 46px;
    margin-right: 15px;
    vertical-align: top;
    border: 0;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
}

.card .card-heading.image .card-heading-header {
    display: inline-block;
    vertical-align: top;
}

.card .card-heading.image .card-heading-header h3 {
    margin: 0;
    font-size: 14px;
    line-height: 16px;
    color: #262626;
}

.card .card-heading.image .card-heading-header span {
    font-size: 12px;
    color: #999999;
}

.card .card-body {
    padding: 0 20px;
    margin-top: 20px;
}

.card .card-media {
    padding: 0 20px;
    margin: 0 -14px;
}

.card .card-media img {
    max-width: 100%;
    max-height: 100%;
}

.card .card-actions {
    min-height: 30px;
    padding: 0 20px 20px 20px;
    margin: 20px 0 0 0;
}

.card .card-comments {
    padding: 20px;
    margin: 0;
    background-color: #f8f8f8;
}

.card .card-comments .comments-collapse-toggle {
    padding: 0;
    margin: 0 20px 12px 20px;
}

.card .card-comments .comments-collapse-toggle a,
.card .card-comments .comments-collapse-toggle span {
    padding-right: 5px;
    overflow: hidden;
    font-size: 12px;
    color: #999;
    text-overflow: ellipsis;
    white-space: nowrap;
}

.card-comments .media-heading {
    font-size: 13px;
    font-weight: bold;
}

.card.people {
    position: relative;
    display: inline-block;
    width: 170px;
    height: 300px;
    padding-top: 0;
    margin-left: 20px;
    overflow: hidden;
    vertical-align: top;
}

.card.people:first-child {
    margin-left: 0;
}

.card.people .card-top {
    position: absolute;
    top: 0;
    left: 0;
    display: inline-block;
    width: 170px;
    height: 150px;
    background-color: #ffffff;
}

.card.people .card-top.green {
    background-color: #53a93f;
}

.card.people .card-top.blue {
    background-color: #427fed;
}

.card.people .card-info {
    position: absolute;
    top: 150px;
    display: inline-block;
    width: 100%;
    height: 101px;
    overflow: hidden;
    background: #ffffff;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.card.people .card-info .title {
    display: block;
    margin: 8px 14px 0 14px;
    overflow: hidden;
    font-size: 16px;
    font-weight: bold;
    line-height: 18px;
    color: #404040;
}

.card.people .card-info .desc {
    display: block;
    margin: 8px 14px 0 14px;
    overflow: hidden;
    font-size: 12px;
    line-height: 16px;
    color: #737373;
    text-overflow: ellipsis;
}

.card.people .card-bottom {
    position: absolute;
    bottom: 0;
    left: 0;
    display: inline-block;
    width: 100%;
    padding: 10px 20px;
    line-height: 29px;
    text-align: center;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.card.hovercard {
    position: relative;
    padding-top: 0;
    overflow: hidden;
    text-align: center;
    background-color: rgba(214, 224, 226, 0.2);
}

.card.hovercard .cardheader {
    background: url("<?php echo e(url('public/img/usuarios/user.png')); ?>");
    background-size: cover;
    height: 135px;
}

.card.hovercard .avatar {
    position: relative;
    top: -50px;
    margin-bottom: -50px;
}

.card.hovercard .avatar img {
    width: 100px;
    height: 100px;
    max-width: 100px;
    max-height: 100px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
    border: 5px solid rgba(255,255,255,0.5);
}

.card.hovercard .info {
    padding: 4px 8px 10px;
}

.card.hovercard .info .title {
    margin-bottom: 4px;
    font-size: 24px;
    line-height: 1;
    color: #262626;
    vertical-align: middle;
}

.card.hovercard .info .desc {
    overflow: hidden;
    font-size: 12px;
    line-height: 20px;
    color: #737373;
    text-overflow: ellipsis;
}

.card.hovercard .bottom {
    padding: 0 20px;
    margin-bottom: 17px;
}



</style>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('consultapsuv::layouts.plantilla', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>