<?php
    $ci = 19729038;
?>

<?php $__env->startSection('content'); ?>
    <br><br>
    <div class="container">
        <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6" id="buscardor1">
            <div class="input-group stylish-input-group">
                <input type="text" class="form-control buscador"  placeholder="Introducir el número de cédula a buscar" id="ci" >
                <span class="input-group-addon">
                    <button type="submit" id="busqueda_ci_b">
                        <span class="glyphicon glyphicon-search" id="busqueda_ci"></span>
                    </button>
                </span>
            </div>
        </div>
        <div class="col-xs-12 col-lg-6 col-md-6 col-sm-6" id="buscardor2">

            <div class="input-group stylish-input-group">
                <input type="text" class="form-control buscador"  placeholder="Introducir el serial del carnet de la patria" id="carnet" >
                <span class="input-group-addon">
                    <button type="submit">
                        <span class="glyphicon glyphicon-search" id="busqueda_carnet"></span>
                    </button>
                </span>
            </div>
        </div>
    </div>
    <br><br><br><br>
    <div id="consul_datos">
    <div class="container">
    <div class="row">

        <div class="col-xs-12  col-md-4">
            <div class="col-xs-12  col-md-12">
                <div class="widget-author  boxed  push-down-30">
                    <div class="widget-author__image-container">
                        <div class="widget-author__avatar--blurred">


                                <img id="foto" src="<?php echo e(url('public/img/usuarios/user.png')); ?>" alt="Avatar image" width="90" height="90">


                        </div>

                            <img  id="foto_" class="widget-author__avatar" src="<?php echo e(url('public/img/usuarios/user.png')); ?>" onerror="this.src = '<?php echo e(url('public/img/usuarios/user.png')); ?>'" alt="Avatar image" width="90" height="90">

                    </div>
                    <div class="row">
                        <div class="col-xs-10  col-xs-offset-1">
                            <br><br><br>
                        </div>
                        <div class="xxx">
                            <div class="col-md-12 col-xs-12">
                                <div class="contenedor well-menu" data-tipo="1">
                                    <div class="well2" data-tipo="1">
                                        <center><h3>Informacion Personal</h3></center>
                                        <table class="table table-striped table-bordered table-hover table-condensed" style=" text-align: left; ">
                                            <tbody>
                                                <tr>
                                                    <td class="b"> <center><b>Nombres y Apellidos</b></center></td>
                                                    <td id ="nombr_"><!--  --></td>
                                                </tr>

                                                <tr>
                                                    <td><center><b>Cédula </b></center></td>
                                                    <td id ="dni_"><!--   --></td>
                                                </tr>

                                                <tr>
                                                    <td> <center><b>Fecha de Nacimiento </b></center></td>
                                                    <td id="fech_nac_"><!--  --></td>
                                                </tr>

                                                <tr>
                                                    <td> <center><b>Edad </b></center></td>
                                                    <td id="edad_"> <!--   --></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="contenedor well-menu" data-tipo="2">
                                    <div class="well2" data-tipo="2">
                                        <table class="table table-striped table-bordered table-hover table-condensed" style=" text-align: left;">
                                            <tbody>
                                                <tr>
                                                    <td><center><b>Municipio:</b></center></td>
                                                    <td id="municipio_"></td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Parroquia:</b></center></td>
                                                    <td id="parroquia_"></td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Direccion:</b></center></td>
                                                    <td id="direccion_"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="contenedor well-menu" data-tipo="3">
                                    <div class="well2" data-tipo="3">
                                        <center><h3>Contacto</h3></center>
                                        <table class="table table-striped table-bordered table-hover table-condensed" style=" text-align: left; ">
                                            <tbody>
                                                <tr>
                                                    <td><center><b>Teléfono:</b></td>
                                                    <td id="telf_"></td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Correo:</b></td>
                                                    <td id="correo_"></td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Twitter:</b></td>
                                                    <td id="tw_"></td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Instagram:</b></td>
                                                    <td id="ig_"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="contenedor well-menu" data-tipo="5">
                                    <div class="well2" data-tipo="4">
                                        <center><h3>Carnet de la patria</h3></center>
                                        <table class="table table-striped table-bordered table-hover table-condensed" style=" text-align: left; ">
                                            <tr>
                                                <td><center><b>Carnet de la patria</b></center></td>
                                                <td id="cp_">
                                                    
                                                </td>
                                            </tr>
                                            <tr>
                                                <td><center><b>Estatus:</b></center></td>
                                                <td id="estatus_"></td>
                                            </tr>
                                            <tr>
                                                <td><center><b>Sub Ambito:</b></center></td>
                                                <td id="subambito_"></td>
                                            </tr>
                                            <tr>
                                                <td><center><b>Parentesco:</b> </center></td>
                                                <td id="parentesco_"></td>
                                            </tr>
                                            <tr>
                                                <td><center><b>Ambito Especifico:</b></center></td>
                                                <td id="ambespec_"></td>
                                            </tr>
                                            <tr>
                                                <td> <center><b>Sub Ambito:</b></center> </td>
                                                <td id="subambito_"></td>
                                            </tr>

                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12  col-md-8">
            <div class="col-xs-12  col-md-12">
                <div class="panel-group accordion" id="accordion5">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1"><center><h3 class="panel-title">Datos Electorales</h3></center> </a>
                            </h4>
                        </div>
                        <div id="collapse_3_1" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="boxed  push-down-45">
                                    <div class="meta">
                                        <div class="psuv">
                                            <div class="col-xs-12  col-sm-4">
                                                <div class="col-xs-12  col-sm-10  col-sm-offset-1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="post-content--front-page" style=" padding: 15px;">
                                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                                                        <!--Datos Electorales-->
                                                        <div class="col-md-10 titulo_demostracion">
                                                            Grafico Tendencia Mesa Donde Vota
                                                        </div>
                                                        <div class="col-md-12" style="height: 3px; background: #d90416;"></div>
                                                        <div id="psuv" class="col-md-12 fondo">
                                                            <div class="col-md-12 col-xs-12 col-sm-12">
                                                                <div id="grafica_centro" ></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="boxed  push-down-45">
                                    <div class="meta">
                                        <div class="psuv">
                                            <div class="col-xs-12  col-sm-4">
                                                <div class="col-xs-12  col-sm-10  col-sm-offset-1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="post-content--front-page" style=" padding: 15px;">
                                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                                                        <!--Datos Electorales-->
                                                        <div class="col-md-10 titulo_demostracion">
                                                            Datos Electorales Actualizados
                                                        </div>
                                                        <div class="col-md-12" style="height: 3px; background: #d90416;"></div>
                                                        <div id="psuv" class="col-md-12 fondo">
                                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                                <div class="col-md-12 col-xs-9">
                                                                    <br>

                                                                    
                                                                        <img id="v7oct_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Voto Elecciones Chavez 2012</b> <br />

                                                                    
                                                                        <img id="v14abr_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Voto Elecciones Maduro 2013</b> <br />

                                                                    
                                                                        <img id="v28jul_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Voto Primaria PSUV (AN):</b>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                                <div class="col-md-12 col-xs-9">
                                                                    <br>
                                                                    
                                                                        <img id="primud_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Voto Primaria de la MUD</b> <br />

                                                                    
                                                                        <img id="v6d_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    
                                                                    <b>Voto Asamblea Nacional</b> <br />

                                                                    
                                                                        <img id="ubch_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    
                                                                    <b>Miembro UBCH</b>

                                                                    <hr>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                                <div class="col-md-12 col-xs-9">

                                                                    
                                                                        <img id="firma_chavez_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    
                                                                    <b>Firmo Referendum de Chavez</b> <br />

                                                                    
                                                                        <img id="firma_maduro_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Firmo Referendum de Maduro</b> <br>  <br>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- fin de cheka -->



                                <!-- inic -->
                                <div class="boxed  push-down-45">
                                    <div class="meta">
                                        <div class="psuv">
                                            <div class="col-xs-12  col-sm-4">
                                                <div class="col-xs-12  col-sm-10  col-sm-offset-1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="post-content--front-page" style=" padding: 15px;">
                                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                                                        <!--Datos del centro Electoral-->
                                                        <div class="col-md-10 titulo_demostracion">
                                                            Datos Del Centro Electoral
                                                        </div>
                                                        <div class="col-md-12" style="height: 3px; background: #d90416;"></div>
                                                        <div id="psuv" class="col-md-12 fondo">
                                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                                <div class="col-md-12 col-xs-9">
                                                                    <br>
                                                                    <b>Codigo del centro Electrol:</b> <p id="cod_cnt_elect_"></p>   <br>
                                                                    <b>Nombre del Centro Electoral:</b> <p id="nbr_cnt_elect_"></p>  <br>
                                                                    <b>N° Mesa donde Vota:</b> <p id="n_mesa_"></p> <br>
                                                                    <b>Tecnologia de la Mesa:</b> <p id="t_mesa_"></p><br>
                                                                    <b>Elctores:</b> <p id="elect_"></p><br>
                                                                    <b>Militantes Psuv / REP:</b> <p id="mil_psuv_rep_"></p> <br> <br>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                                <div class="col-md-12 col-xs-9">
                                                                    <br>
                                                                    <b>Firmaron:</b><p id="firmaron_"></p><br>
                                                                    <b>Registrado 2011 como</b><p id="reg_2011_"></p>  <br>
                                                                    <b>Direccion:</b><p id="direc_centro_"></p>  <br>
                                                                    <b>Parroquia:</b><p id="parroquia_centro_"></p> <br>
                                                                    <b>Municipio:</b><p id="municipio_centro_"></p> <br> <br>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- fn -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12  col-md-12">
                <div class="panel-group accordion" id="accordion5">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2"><center><h3 class="panel-title">Datos Politicos</h3></center> </a>
                            </h4>
                        </div>
                        <div id="collapse_3_2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="boxed  push-down-45">
                                    <div class="meta">
                                        <div class="psuv">
                                            <div class="col-xs-12  col-sm-4">
                                                <div class="col-xs-12  col-sm-10  col-sm-offset-1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="post-content--front-page" style=" padding: 15px;">
                                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                                                        <div class="col-md-2 col-xs-12 col-sm-12 titulo_demostracion">PSUV</div>
                                                        <div class="col-md-12 col-xs-12 col-sm-12" style="height: 3px; background: #d90416;"></div>
                                                        <div id="psuv" class="col-md-12 fondo">
                                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                                <div class="col-md-12 col-xs-9">
                                                                    <br>
                                                                    
                                                                        <img id="inscrito_psuv_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Inscrito PSUV</b>  </br>

                                                                    
                                                                        <img id="inscrito_2007_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Inscrito 2007</b> </br>

                                                                    
                                                                        <img id="inscrito_2009_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Inscrito 2009</b> </br>

                                                                    
                                                                        <img id="actualizado_2009_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Actualizado 2009</b> </br>

                                                                    
                                                                        <img id="inscrito_2010_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Inscrito 2010</b> </br>

                                                                    <hr>
                                                                    
                                                                        <img id="hogares_patria_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Pertenece a Hogares Patria 2013</b> </br>

                                                                    
                                                                        <img id="cargo_cce_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Participo como Equipo C.C.E 2013</b>  </br>

                                                                    
                                                                        <img id="cargo_ccm_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Participo como Equipo C.C.M 2013</b> <br>

                                                                    
                                                                        <img id="cargo_ccp" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Participo como Equipo C.C.P 2013</b> <br>

                                                                    
                                                                        <img id="anillo_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                    <b>Anillo N&uacutemero: </b> <p id="anillo_t"></p>  <br>


                                                                    
                                                                        <img id="1x10_resp_centro_" src='<?php echo e(url("public/img/check2.png")); ?>' style='width: 24px; height: 24px;' />
                                                                        <b>Responsable 1x10 Centro: </b> <a id="modal_centro" style="cursor: pointer;"><p id="1x10_resp_centro_t"></p></a>  <br>


                                                                    <hr>
                                                                    
                                                                        <img id="pat_terr_" src='<?php echo e(url("public/img/check.png")); ?>' style='width: 24px; height: 24px;' />
                                                                        <b>Patrulla Territorial: </b> <a id="modal_territoriales_m" style="cursor: pointer;"> <p id="cod_patrulla_terr_"></p> </a><br>
                                                                        
                                                                            <b>Responsable: </b><p id="responsable_patrulla"></p><br/>
                                                                        
                                                                            
                                                                        

                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- fin de cheka -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12  col-md-12">
                <div class="panel-group accordion" id="accordion5">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle accordion-toggle-styled collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3"><center><h3 class="panel-title">Ayudas Sociales</h3></center> </a>
                            </h4>
                        </div>
                        <div id="collapse_3_3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="boxed  push-down-45">
                                    <div class="meta">
                                        <div class="psuv">
                                            <div class="col-xs-12  col-sm-4">
                                                <div class="col-xs-12  col-sm-10  col-sm-offset-1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <div class="post-content--front-page" style=" padding: 15px;">
                                                    <div class="col-md-12 col-sm-12 col-xs-12" style="margin-top: 3%;">
                                                        <div class="col-md-2 col-xs-12 col-sm-12 titulo_demostracion">Ayudas</div>
                                                        <div class="col-md-12 col-xs-12 col-sm-12" style="height: 3px; background: #d90416;"></div>
                                                        <div id="psuv" class="col-md-12 fondo">
                                                            <div class="col-md-6 col-xs-12 col-sm-6">
                                                                <div class="col-md-12 col-xs-9">
                                                                    <br>
                                                                    <br>
                                                                    <b>Ultima ayuda solicitada: </b> <p id="ultima_ayuda"></p> <br><br>
                                                                    <b>Observacion: </b> <p id="observacion_ayuda"></p> <br>
                                                                    <b>Tipo de Ayuda: </b> <p id="tipo_ayuda"></p> <br>
                                                                    <b>Total de Solicitudes: </b> <p id="ayudas_solicitadas"></p> <br>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- fin de cheka -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12  col-md-12">
                
            </div>
        </div>

    </div>
</div>
    </div>




<?php $__env->stopSection(); ?>
<?php $__env->startPush('js'); ?>
<script type="text/javascript">

</script>
<?php $__env->stopPush(); ?>
<?php $__env->startPush('css'); ?>
<style>

.card {
    padding-top: 20px;
    margin: 10px 0 20px 0;
    background-color: rgba(214, 224, 226, 0.2);
    border-top-width: 0;
    border-bottom-width: 2px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}
.btn {
    width: 100%;!important;
    height: 32px;
    line-height: 18px;
}
.card .card-heading {
    padding: 0 20px;
    margin: 0;
}

.card .card-heading.simple {
    font-size: 20px;
    font-weight: 300;
    color: #777;
    border-bottom: 1px solid #e5e5e5;
}

.card .card-heading.image img {
    display: inline-block;
    width: 46px;
    height: 46px;
    margin-right: 15px;
    vertical-align: top;
    border: 0;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
}

.card .card-heading.image .card-heading-header {
    display: inline-block;
    vertical-align: top;
}

.card .card-heading.image .card-heading-header h3 {
    margin: 0;
    font-size: 14px;
    line-height: 16px;
    color: #262626;
}

.card .card-heading.image .card-heading-header span {
    font-size: 12px;
    color: #999999;
}

.card .card-body {
    padding: 0 20px;
    margin-top: 20px;
}

.card .card-media {
    padding: 0 20px;
    margin: 0 -14px;
}

.card .card-media img {
    max-width: 100%;
    max-height: 100%;
}

.card .card-actions {
    min-height: 30px;
    padding: 0 20px 20px 20px;
    margin: 20px 0 0 0;
}

.card .card-comments {
    padding: 20px;
    margin: 0;
    background-color: #f8f8f8;
}

.card .card-comments .comments-collapse-toggle {
    padding: 0;
    margin: 0 20px 12px 20px;
}

.card .card-comments .comments-collapse-toggle a,
.card .card-comments .comments-collapse-toggle span {
    padding-right: 5px;
    overflow: hidden;
    font-size: 12px;
    color: #999;
    text-overflow: ellipsis;
    white-space: nowrap;
}

.card-comments .media-heading {
    font-size: 13px;
    font-weight: bold;
}

.card.people {
    position: relative;
    display: inline-block;
    width: 170px;
    height: 300px;
    padding-top: 0;
    margin-left: 20px;
    overflow: hidden;
    vertical-align: top;
}

.card.people:first-child {
    margin-left: 0;
}

.card.people .card-top {
    position: absolute;
    top: 0;
    left: 0;
    display: inline-block;
    width: 170px;
    height: 150px;
    background-color: #ffffff;
}

.card.people .card-top.green {
    background-color: #53a93f;
}

.card.people .card-top.blue {
    background-color: #427fed;
}

.card.people .card-info {
    position: absolute;
    top: 150px;
    display: inline-block;
    width: 100%;
    height: 101px;
    overflow: hidden;
    background: #ffffff;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.card.people .card-info .title {
    display: block;
    margin: 8px 14px 0 14px;
    overflow: hidden;
    font-size: 16px;
    font-weight: bold;
    line-height: 18px;
    color: #404040;
}

.card.people .card-info .desc {
    display: block;
    margin: 8px 14px 0 14px;
    overflow: hidden;
    font-size: 12px;
    line-height: 16px;
    color: #737373;
    text-overflow: ellipsis;
}

.card.people .card-bottom {
    position: absolute;
    bottom: 0;
    left: 0;
    display: inline-block;
    width: 100%;
    padding: 10px 20px;
    line-height: 29px;
    text-align: center;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.card.hovercard {
    position: relative;
    padding-top: 0;
    overflow: hidden;
    text-align: center;
    background-color: rgba(214, 224, 226, 0.2);
}

.card.hovercard .cardheader {
    background: url("<?php echo e(url('public/img/usuarios/user.png')); ?>");
    background-size: cover;
    height: 135px;
}

.card.hovercard .avatar {
    position: relative;
    top: -50px;
    margin-bottom: -50px;
}

.card.hovercard .avatar img {
    width: 100px;
    height: 100px;
    max-width: 100px;
    max-height: 100px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
    border: 5px solid rgba(255,255,255,0.5);
}

.card.hovercard .info {
    padding: 4px 8px 10px;
}

.card.hovercard .info .title {
    margin-bottom: 4px;
    font-size: 24px;
    line-height: 1;
    color: #262626;
    vertical-align: middle;
}

.card.hovercard .info .desc {
    overflow: hidden;
    font-size: 12px;
    line-height: 20px;
    color: #737373;
    text-overflow: ellipsis;
}

.card.hovercard .bottom {
    padding: 0 20px;
    margin-bottom: 17px;
}



</style>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('consultapsuv::layouts.plantilla', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>