    <div id="header" class="header-area header-one">
        

        <div class="logo-area">
            <div class="container">
                <div class="row">
                    <div class="main-logo col-xs-12  col-lg-12 col-md-12 col-sm-12  pd-top-20 pd-bottom-20">
                        <div class="logo">
                            <a href="<?php echo e(url('/')); ?>"><img class="retina img-responsive " src="<?php echo e(asset('public/img/logos/'.$controller->conf('logo'))); ?>" alt="logo" style="width: 145px;"></a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    
     <?php echo $__env->make('consultapsuv::partials.menu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   