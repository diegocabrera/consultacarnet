	<div id="main-nav" class="main-nav">
        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div> 
                <div class="collapse navbar-collapse" id="main-navbar">
                    <ul class="nav navbar-nav main-navbar navbar-left">
                        
						<?php echo \App\Modules\Base\Models\Menu::generar_menu2($controller->app); ?>

                    </ul>
                    <ul class="nav navbar-nav navbar-right ">
                        <li class="search-menu-btn">
                            <a class="search-buttom" href="<?php echo e(url(Config::get('admin.prefix').'/login/salir')); ?>" title="Salir" ><i class="fa fa-power-off fa-stack-1x"></i></a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>