<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $__env->make('consultapsuv::partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <!--Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lalezar&amp;subset=arabic,latin-ext,vietnamese" rel="stylesheet">
      
    </head>
    <body>
        <?php echo $__env->make('consultapsuv::partials.page-header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
        
        <div class="container-top">
            <?php echo $__env->yieldContent('content-top'); ?>
        </div>
            <?php echo $__env->yieldContent('content'); ?>
        <div class="container-botton"></div>
        <?php echo $__env->make('consultapsuv::partials.page-footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?> 
      
        <?php echo $__env->make('consultapsuv::partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  
    </body>
</html>
